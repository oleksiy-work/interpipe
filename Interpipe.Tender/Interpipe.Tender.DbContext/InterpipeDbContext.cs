﻿using Interpipe.Tender.Models;
using Microsoft.EntityFrameworkCore;

namespace Interpipe.Tender.DbContext
{
    public class InterpipeDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public InterpipeDbContext(DbContextOptions<InterpipeDbContext> options)
          : base(options)
        {

        }

        public virtual DbSet<Models.Tender> Tenders { get; set; }

        public virtual DbSet<Bid> Bids { get; set; }

        public virtual DbSet<BidHistory> BidHistory { get; set; }
    }
}
