using Interpipe.Tender.DbContext;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using Interpipe.Tender.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Interpipe.Tender.Services.Auth;
using Interpipe.Tender.Options;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Caching.Memory;
using Interpipe.Tender.Hubs;
using Interpipe.Tender.BackgroundTasks;
using Interpipe.Tender.Constants;
using Interpipe.Tender.Services.Localization;

namespace Interpipe.Tender
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(Configuration.GetConnectionString("DefaultConnection"))
            {
                MaxPoolSize = (int) MaxPoolSize.Sql,
            };
            services.AddDbContextPool<InterpipeDbContext>(options =>
                options.UseSqlServer(
                    builder.ToString(), sqlServerOptionsAction: action => action.EnableRetryOnFailure(
                                          maxRetryCount: 3, maxRetryDelay: TimeSpan.FromSeconds(10), errorNumbersToAdd: null)));


            SqlConnectionStringBuilder bidsConnectionBuilder = new SqlConnectionStringBuilder(Configuration.GetConnectionString("BidsConnection"))
            {
                MaxPoolSize = (int) MaxPoolSize.Sql,
            };
            
            services.AddDbContextPool<Interpipe.Tender.Bids.DbContext.BidsDbContext>(options =>
               options.UseSqlServer(
                   bidsConnectionBuilder.ToString(), sqlServerOptionsAction: action => action.EnableRetryOnFailure(
                                         maxRetryCount: 3, maxRetryDelay: TimeSpan.FromSeconds(10), errorNumbersToAdd: null)));

            var syncOptions = Configuration.GetSection("BackgroundSyncTaskOptions");

            services.Configure<BackgroundSyncTaskOptions>(syncOptions);

            services.AddControllersWithViews(options=> {
                //options.Filters.Add<RefererRequesFilter>();
            });
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            // services.AddHostedService<BidsSyncBackgroundService>();
            services.Configure<InterpipeOptions>(Configuration.GetSection("InterpipeOptions"));

            services.AddTransient<ITenderService, TenderService>();
            services.AddTransient<IAuthService, AuthService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(opt=>
                    {
                        opt.Cookie.Name = "_hash";
                        opt.LoginPath = "/";
                    });

            services.AddSignalR(hubOptions =>
            {
                hubOptions.EnableDetailedErrors = true;
                hubOptions.HandshakeTimeout = TimeSpan.FromMinutes(5);

            });

            services.AddMemoryCache(options =>
            {
                if (options is null)
                {
                    throw new ArgumentNullException(nameof(options));
                }

                new MemoryCacheEntryOptions().SetSize(1024);
            });

            services.AddSingleton<ILocalizationService, LocalizationService>();
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
            };

            app.UseCookiePolicy(cookiePolicyOptions);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapHub<PricesHub>("/prices");
                endpoints.MapHub<TenderTimeHub>("/tendertimehub");
            });
            var options = Configuration.GetSection("InterpipeOptions");
            
            app.Use(async (context, next) => {

                if (!IsRequestAllowed(context.Request, options))
                {
                    context.Response.StatusCode = 404;
                    return;
                }

                await next.Invoke();
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

           
           
        }

        private static bool IsRequestAllowed(HttpRequest request, IConfigurationSection options)
        {

            var referer = request.Headers["Referer"].ToString();

            if (string.IsNullOrEmpty(referer))
                return true;

            var domain = request.Host.Value;
            var mainSite = options.GetValue<string>("MainSiteUrl");
            return referer == domain || referer.Contains(domain, StringComparison.InvariantCultureIgnoreCase)
                || referer == mainSite || referer.Contains(mainSite, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
