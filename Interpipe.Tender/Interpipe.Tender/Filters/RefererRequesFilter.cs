﻿using Interpipe.Tender.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Interpipe.Tender.Filters
{

    public class RefererRequesFilter : IAsyncActionFilter
    {
        private readonly InterpipeOptions _options;

        public RefererRequesFilter(IOptions<InterpipeOptions> options)
        {
            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            _options = options.Value;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context,
                                    ActionExecutionDelegate next)
        {
            if(!IsRequestAllowed(context.HttpContext.Request))
            {
                context.Result = new RedirectResult("https://google.com");
                return;
            }
            
            // код метода
            await next();
        }

        private bool IsRequestAllowed(HttpRequest request)
        {

            var referer = request.Headers["Referer"].ToString();

            var domain = request.Host.Value;

            return referer == domain || referer.Contains(domain, StringComparison.InvariantCultureIgnoreCase)
                || referer == _options.MainSiteUrl || referer.Contains(_options.MainSiteUrl, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
