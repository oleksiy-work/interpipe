﻿namespace Interpipe.Tender.Options
{
    public class InterpipeOptions
    {
        public string MainSiteUrl { get; set; }

        public bool AllowOnlyConnectionFormMainSite { get; set; }

        public int AuthCookieExpiresInMinutes { get; set; }
    }
}
