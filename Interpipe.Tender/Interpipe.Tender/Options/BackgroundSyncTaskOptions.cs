﻿namespace Interpipe.Tender.Options
{
    public class BackgroundSyncTaskOptions
    {
        public int BackgroundTaskExecutingTimeInSeconds { get; set; }
    }
}
