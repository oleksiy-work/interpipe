﻿using Interpipe.Tender.Constants;
using Interpipe.Tender.Extensions;
using Interpipe.Tender.Helpers;
using Interpipe.Tender.Services;
using Interpipe.Tender.ViewModels;
using Interpipe.Tender.ViewModels.Bids;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.Controllers
{
    [Authorize]
    [Route("api/bids")]
    [ApiController]
    public class BidsController : ControllerBase
    {
        private readonly ITenderService _tenderService;
        // private readonly OperationLogger _ol;

        // private readonly ILogger<BidsController> _logger;
        private TenderTypeResolver tenderTypeResolver = new TenderTypeResolver();
        public BidsController(ITenderService tenderService
            // ILogger<BidsController> logger
            )
        {
            _tenderService = tenderService ?? throw new ArgumentNullException(nameof(tenderService));
            // _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            // _ol = new OperationLogger(_logger);
        }


        [HttpPost("selected-bids-info")]
        [Produces("application/json")]
        public async Task<IActionResult> BuildBidsList([FromBody] IEnumerable<BuildBidsViewModel> model)
        {
            //_ol.Log("BuildBidsList");
            //_ol.StartMeasureExecutionTime();

            var selectedTenders = (await _tenderService.GetTendersAsync(HttpContext.User.Identity.Name))
                .Where(x => model.Any(a => (a.LotNumber == x.LotNumber && a.UnicalNumber == x.UnicalNumber) || (a.TenderBackUnicalNumber ?? -1) == x.UnicalNumber))
                .ToList();

            var countriesFrom = selectedTenders.Select(x => new { name = x.CountryFrom.Trim(), iso = x.CountryFromISO }).Distinct().ToList();
            var countriesTo = selectedTenders.Select(x => new { name = x.CountryTo.Trim(), iso = x.CountryToISO }).Distinct().ToList();
            var citiesFrom = selectedTenders.Select(x => new { name = x.CityFrom.Trim(), countryIso = x.CountryFromISO }).Distinct().ToList();
            var citiesTo = selectedTenders.Select(x => new { name = x.CityTo.Trim(), countryIso = x.CountryToISO }).Distinct().ToList();
            var currencies = selectedTenders.Select(x => x.Currency).Distinct().ToList();

            List<BidInformationViewModel> result = new List<BidInformationViewModel>();

            foreach (var tender in selectedTenders)
            {
                var tenderType = tenderTypeResolver.GetTenderTypeId(tender?.TenderType);
                var view = await MakeBidInfo(tender, HttpContext.User.Identity.Name);
                
                if (tenderType == TenderTypeId.Contract)
                {
                    if (tender != null)
                    {
                        var tenderStartPrice = tender.MinPrice;
                        if (view.Price <= tenderStartPrice)
                        {
                            var minPrice = await _tenderService.GetMinBidPriceConAsync(tender.UnicalNumber);
                            view.Price = minPrice == 0 ? tenderStartPrice : view.Price;
                        }   
                        else
                        {
                            view.Price = tenderStartPrice;
                        }
                    }
                }
                result.Add(view);
            }

            //_ol.LogExecutionTime();

            return Ok(new
            {
                bids = result,
                filters = new
                {
                    countriesFrom,
                    countriesTo,
                    citiesFrom,
                    citiesTo,
                    currencies
                },
            });
        }


        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> MakeBid([FromBody] IEnumerable<MakeBidViewModel> model)
        {
            //_ol.StartMeasureExecutionTime(); 
            //_ol.Log("MakeBid");

            List<MakeBidError> errors = new List<MakeBidError>();
            var bidsResult = await _tenderService.MakeBidsAsync(model.Select(x => new Models.Bid
            {
                Login = HttpContext.User.Identity.Name,
                UnicalNumber = x.UnicalNumber,
                LotNumber = x.LotNumber,
                Price = x.Price,
                Currenty = x.Currency,
                Comment = x.Comment,
                IpAddress = HttpContext.Connection.RemoteIpAddress.ToString(),
                BrowserInfo = Request.Headers["User-Agent"].ToString(),
                SaveDate = DateTime.Now,
                VehiclesAmount = x.VehiclesAmount ?? 1
            }), errors);

                //_ol.LogExecutionTime();

            if (bidsResult && errors.Count == 0)
                return Ok();

            return BadRequest(errors);
        }

        private async Task<BidInformationViewModel> MakeBidInfo(Models.Tender tender, string userName)
        {
            var bid = await _tenderService.GetBidHistory(HttpContext.User.Identity.Name, tender.UnicalNumber, tender.LotNumber);
            return new BidInformationViewModel
            {
                AdditionalInfo = tender.CustomerComment,
                LotNumber = tender.LotNumber,
                UnicalNumber = tender.UnicalNumber,
                Number = tender.Number,
                CargoType = tender.CargoType,
                City = tender.CityTo,
                Comment = bid?.Comment,
                CountryTo = tender.CountryTo,
                Currency = tender.Currency,
                CurrenPrice = tender.MinPrice,
                LoadingFrom = tender.LoadingFrom,
                LoadingTo = tender.LoadingTo,
                MyPrice = bid?.Price,
                PaymentConditions = tender.PaymentConditions,
                PriceStep = tender.PriceStep,
                TenderEndTime = tender.EndTime,
                TransportType = tender.TransportType,
                UnloadDate = tender.UnloadingDate,
                CityFrom = tender.CityFrom,
                IsReduction = Convert.ToBoolean(tender.IsReduction),
                CloseAfter = (tender.EndTime - DateTime.Now).TotalMilliseconds,
                Price = await _tenderService.GetBestPrice(tender.UnicalNumber, tender.LotNumber) ?? tender.MinPrice,
                CountryFrom = tender.CountryFrom,
                Status = tender.Status,
                TenderBackUnicalNumber = tender.BackShippingTender,
                CarsAmount = tender.VehiclesAmount,
                CityTo = tender.CityTo,
                CountryFromISO = tender.CountryFromISO,
                CountryToISO = tender.CountryToISO,
                TenderType = tender.TenderType,
                LoadingFromInMs = tender.LoadingFrom.ToUnixTime(),
                UnloadingInMs = tender.UnloadingDate.ToUnixTime(),
                MyCarsAmount = bid?.VehiclesAmount ?? 0,
                IsFinished = (tender.EndTime - DateTime.Now).TotalMilliseconds <= 0,
                EndTimeUtc = tender.EndTime.ToUniversalTime(),
                ServerTime = DateTime.UtcNow,
            };
        }
    }
}
