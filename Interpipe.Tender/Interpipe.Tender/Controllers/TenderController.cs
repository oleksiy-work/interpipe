﻿using Interpipe.Tender.Constants;
using Interpipe.Tender.Extensions;
using Interpipe.Tender.Services;
using Interpipe.Tender.ViewModels.Tenders;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.Controllers
{
    [Route("api/tenders")]
    [ApiController]
    public class TenderController : ControllerBase
    {
        // private readonly OperationLogger _ol;
        private readonly ITenderService _tenderService;
        // private readonly InterpipeOptions _options;
        // private readonly ILogger<TenderController> _logger;
        public TenderController(ITenderService tenderService
            //IOptions<InterpipeOptions> options
            //ILogger<TenderController> logger
            )
        {
            _tenderService = tenderService ?? throw new ArgumentNullException(nameof(tenderService));
            // _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            //if (options is null)
            //{
            //    throw new ArgumentNullException(nameof(options));
            //}

            //_options = options.Value;

            // _ol = new OperationLogger(_logger);
        }

        [HttpGet()]
        public async Task<IActionResult> GetTenders()
        {

            //_ol.Log("GetTenders");
            //_ol.StartMeasureExecutionTime();

            var list = (await _tenderService.GetTendersAsync(
                HttpContext.User.Identity.IsAuthenticated
                ? HttpContext.User.Identity.Name
                : string.Empty)).ToList();

            var grouped = list.GroupBy(x => x.CountryTo)
                .ToList();

            var countriesFrom = list.Select(x => new { name = x.CountryFrom.Trim(), iso = x.CountryFromISO }).Distinct().ToList();
            var countriesTo = list.Select(x => new { name = x.CountryTo.Trim(), iso = x.CountryToISO }).Distinct().ToList();
            var citiesFrom = list.Select(x => new { name = x.CityFrom.Trim(), countryIso = x.CountryFromISO }).Distinct().ToList();
            var citiesTo = list.Select(x => new { name = x.CityTo.Trim(), countryIso = x.CountryToISO }).Distinct().ToList();
            var currencies = list.Select(x => x.Currency).Distinct().ToList();
            var tenders = grouped.Select(x => BuildTendersViewModel(x));

            // _ol.LogExecutionTime();

            return Ok(new
            {
                filters = new
                {
                    countriesFrom,
                    countriesTo,
                    citiesFrom,
                    citiesTo,
                    currencies
                },
                tenders,
            });
        }

        [HttpGet("new")]
        public async Task<IActionResult> GetNewTendersAmount()
        {
            //_ol.StartMeasureExecutionTime();
            //_ol.Log("GetNewTendersAmount");

            string name = HttpContext.User.Identity.IsAuthenticated
                ? HttpContext.User.Identity.Name
                : string.Empty;

            if (string.IsNullOrWhiteSpace(name))
                return Ok(0);

            var newAmount = await _tenderService.NewTendersAmount(name);

            // _ol.LogExecutionTime();

            return Ok(newAmount);
        }
        
        [HttpGet("utcdate")]
        public DateTime GetCurrentDate()
        {
            return DateTime.UtcNow;
        }

        private TenderGroupViewModel BuildTendersViewModel(IGrouping<string, Models.Tender> tendersGroup)
        {
            return new TenderGroupViewModel
            {
                Country = tendersGroup.Key,
                Items = tendersGroup.Select(x => new TenderViewModel
                {
                    CargoType = x.CargoType,
                    CityFrom = x.CityFrom,
                    CityTo = x.CityTo,
                    CountryFrom = x.CountryFrom,
                    CountryFromISO = x.CountryFromISO,
                    CountryTo = x.CountryTo,
                    CountryToISO = x.CountryToISO,
                    CustomerComment = x.CustomerComment,
                    EndTime = x.EndTime,
                    LoadingFrom = x.LoadingFrom,
                    LoadingTo = x.LoadingTo,
                    LotNumber = x.LotNumber,
                    Number = x.Number,
                    TransportType = x.TransportType,
                    TypeName = x.TypeName,
                    UnicalNumber = x.UnicalNumber,
                    UnloadingDate = x.UnloadingDate,
                    currency = x.Currency,
                    TenderType = x.TenderType,
                    TenderBackUnicalNumber = x.BackShippingTender,
                    LoadingFromInMs = x.LoadingFrom.ToUnixTime(),
                    UnloadingInMs = x.UnloadingDate.ToUnixTime()

                })
            };
        }
    }
}