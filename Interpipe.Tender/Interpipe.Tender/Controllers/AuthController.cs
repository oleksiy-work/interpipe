﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Interpipe.Tender.Services.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Interpipe.Tender.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        // private readonly ILogger<AuthController> _logger;

        public AuthController(IAuthService authService 
            // ILogger<AuthController> logger
            )
        {
            _authService = authService ?? throw new ArgumentNullException(nameof(authService));
            // _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<IActionResult> Login([FromQuery]Guid? ticket)
        {

            //if (string.IsNullOrEmpty(Request.Headers["Referer"].ToString()))
            //    return NotFound();

            if (!ticket.HasValue || ticket == Guid.Empty)
            {
                await _authService.SignOutAsync();
                return Redirect("/");
            }

            //_logger.LogWarning($"Login attempt from {Request.Headers["Referer"]} with ticket {ticket} ");

            await _authService.SignInAsync(ticket.Value);

            return Redirect("/");
        }

        //FOR DEBUG USE ONLY
#if DEBUG
        [HttpPost("with-email")]
        public async Task<IActionResult> LoginWithEmail([FromBody] LoginModel model)
        {
            var claims = new List<Claim>
                {

                    new Claim(ClaimsIdentity.DefaultNameClaimType, model.Email),
                    new Claim("Date",DateTime.Now.ToString())
                };

            var claimsIdentity = new ClaimsIdentity(
               claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                // Refreshing the authentication session should be allowed.

                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(60),
                // The time at which the authentication ticket expires. A 
                // value set here overrides the ExpireTimeSpan option of 
                // CookieAuthenticationOptions set with AddCookie.

                //IsPersistent = true,
                // Whether the authentication session is persisted across 
                // multiple requests. When used with cookies, controls
                // whether the cookie's lifetime is absolute (matching the
                // lifetime of the authentication ticket) or session-based.

                //IssuedUtc = <DateTimeOffset>,
                // The time at which the authentication ticket was issued.

                //RedirectUri = <string>
                // The full path or absolute URI to be used as an http 
                // redirect response value.
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);

            return Ok();
        }
#endif

        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return Ok();
        }

        [HttpGet("is-authenticated")]
        public IActionResult IsAuthorized()
        {
            return Ok(HttpContext.User.Identity.IsAuthenticated);
        }
    }

    public class LoginModel
    {
        public string Email { get; set; }
    }
}