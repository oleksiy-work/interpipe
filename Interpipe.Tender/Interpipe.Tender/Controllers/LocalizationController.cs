﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interpipe.Tender.Services.Localization;

namespace Interpipe.Tender.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalizationController : ControllerBase
    {
        private readonly ILocalizationService _localizationService;

        public LocalizationController(ILocalizationService localisationService)
        {
            this._localizationService = localisationService;
        }

        [HttpPost("{lang}")]
        public async Task<ActionResult> Localization(string lang)
        {
            await this._localizationService.SetLocalizationAsync(lang);
            return Ok();
        }

    }
}
