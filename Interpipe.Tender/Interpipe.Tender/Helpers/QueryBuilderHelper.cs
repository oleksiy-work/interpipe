﻿using System;
using System.Globalization;
using System.Text;

namespace Interpipe.Tender.Helpers
{
    public class QueryBuilderHelper
    {
        private StringBuilder _query;
        public QueryBuilderHelper(string storedProcName)
        {
            _query = new StringBuilder();
            _query.Append($"exec [dbo].[{storedProcName}] ");
        }

        public QueryBuilderHelper Append(string parameter, object value)
        {
            _query.Append($"@{parameter} = {Convert.ToString(value, CultureInfo.InvariantCulture)}, ");
            return this;
        }

        public QueryBuilderHelper Append(string parameter, string value)
        {
            _query.Append($"@{parameter} = N'{value}', ");
            return this;
        }

        public QueryBuilderHelper AppendLogin(string login)
        {
            return Append("login", login);
        }

        public QueryBuilderHelper AppendUniqueNumber(int number)
        {
            return Append("undoc", number);
        }

        public QueryBuilderHelper AppendLotNumber(int lotNumber)
        {
            return Append("npp", lotNumber);
        }

        public QueryBuilderHelper AppendPrice(decimal price)
        {
            return Append("price", price);
        }

        public QueryBuilderHelper AppendCurrency(string currency)
        {
            return Append("kval", currency);
        }

        public QueryBuilderHelper AppendComment(string comment)
        {
            return Append("comm", comment);
        }

        public QueryBuilderHelper AppendUserType(string type)
        {
            return Append("scm", type);
        }

        public QueryBuilderHelper AppendIp(string userIp)
        {
            return Append("ipstamp", userIp);
        }

        public QueryBuilderHelper AppendBrowserInfo(string browserInfo)
        {
            return Append("browinfo", browserInfo);
        }

        public QueryBuilderHelper AppendVehiclesAmount(int vehicles)
        {
            return Append("kol", vehicles);
        }
		//---- add
		public QueryBuilderHelper AppendDtSave(DateTime dtSave)
		{
			return Append("dtsave", dtSave);
		}
		//----------------
	
        public string Build()
        {
            return _query
                .ToString()
                .Trim(' ', ',');
        }
    }
}
