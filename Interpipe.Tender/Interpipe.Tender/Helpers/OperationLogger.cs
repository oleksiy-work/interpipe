﻿using System.Diagnostics;

namespace Interpipe.Tender.Helpers
{
    internal sealed class OperationLogger
    {
        // private readonly ILogger _logger;

        // private Stopwatch sw;

        public OperationLogger() //ILogger logger)
        {
            //_logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Log(string msg)
        {
            //var dt = DateTime.UtcNow.ToString("G");
            //var logMsg = dt + " Start >>> " + msg;
            // _logger.Log(LogLevel.Information, logMsg);
        }

        public void StartMeasureExecutionTime()
        {
            // sw = new Stopwatch();
        }

        public void LogExecutionTime()
        {
            // sw.Stop();
            // _logger.LogWarning("<<< End. Time taken: {0}ms", sw.Elapsed.TotalMilliseconds);
        }
    }
}
