﻿using Interpipe.Tender.Constants;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Interpipe.Tender.Helpers
{
    public class TenderTypeResolver
    {
        private readonly Dictionary<string, TenderTypeId> tenderTypes = new Dictionary<string, TenderTypeId>(StringComparer.InvariantCultureIgnoreCase);

        public TenderTypeResolver()
        {
            tenderTypes.Add(Constant.TenderTypeSingle, TenderTypeId.Single);
            tenderTypes.Add(Constant.TenderTypeRNG, TenderTypeId.Round);
            tenderTypes.Add(Constant.TenderTypeGRP, TenderTypeId.Group);
            tenderTypes.Add(Constant.TenderTypeCON, TenderTypeId.Contract);
        }

        public TenderTypeId GetTenderTypeId(string tenderType)
        {
            var s = tenderType?.Trim();
            var tenderTypeId = tenderTypes.FirstOrDefault(x => x.Key == s).Value;

            return tenderTypeId;
        }
    }
}
