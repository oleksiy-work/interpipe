﻿using System;

namespace Interpipe.Tender.Constants
{
    public static class Constant
    {
        public const string TenderGetOutGoingData = "_TENDER.GETOUTGOINGDATA";
        public const string TenderListForUser = "TenderList_";
        public const string TenderLastBidTemplate = "TenderBid_{0}_{1}_{2}";
        public const string BidBestPriceTemplate = "TenderBidBest_{0}_{1}";
        public const string TenderTypeSingle = "";
        public const string TenderTypeRNG = "RNG";
        public const string TenderTypeCON = "CON";
        public const string TenderTypeGRP = "GRP";

        public const string LocalizationUk = "UK";
        public const string LocalizationEn = "EN";
        public const string LocalizationRu = "RU";
    }

    public enum TenderTypeId : int
    {
        Single = 1,
        Round = 2, 
        Group = 3,
        Contract = 4,
    }

    public enum MaxPoolSize : int
    {
        Sql = 2000,
    }
}
