﻿using Interpipe.Tender.Bids.DbContext;
using Interpipe.Tender.Options;
using Interpipe.Tender.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime;
using System.Threading;
using System.Threading.Tasks;

namespace Interpipe.Tender.BackgroundTasks
{
    public class BidsSyncBackgroundService : BackgroundService
    {
        private ILogger<BidsSyncBackgroundService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly BackgroundSyncTaskOptions _options;
        public BidsSyncBackgroundService(IServiceScopeFactory serviceScopeFactory,
            ILogger<BidsSyncBackgroundService> logger,
            IOptions<BackgroundSyncTaskOptions> options) : base()
        {
            _serviceScopeFactory = serviceScopeFactory;
             _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            if (options.Value == null || options.Value.BackgroundTaskExecutingTimeInSeconds == 0)
            {
                _options = new BackgroundSyncTaskOptions
                {
                    BackgroundTaskExecutingTimeInSeconds = 60
                };
            }
            else
            {
                _options = options.Value;
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                using var scope = _serviceScopeFactory.CreateScope();
                _logger = scope.ServiceProvider.GetService<ILogger<BidsSyncBackgroundService>>();
                while (!stoppingToken.IsCancellationRequested)
                {
                    var bidsContext = scope.ServiceProvider.GetService<BidsDbContext>();
                    var tenderService = scope.ServiceProvider.GetService<ITenderService>();
                    var unsyncedBids = await bidsContext.Bids.Where(x => !x.IsSynced).ToListAsync(cancellationToken: stoppingToken);
                    if (unsyncedBids.Count > 0)
                    {
                        _logger.LogWarning("Start bids synchronization.");
                        _logger.LogWarning($"{unsyncedBids.Count} bid(s) founded.");

                        foreach (var bid in unsyncedBids)
                        {
                            if (await tenderService.MakeBid(
                                bid.Login,
                                bid.UnicalNumber,
                                bid.LotNumber,
                                bid.Price,
                                bid.Currenty,
                                bid.Comment,
                                bid.IpAddress,
                                string.Empty,
                                bid.BrowserInfo,
                                bid.VehiclesAmount,
                                bid.SaveDate))
                            {

                                bid.IsSynced = true;
                            }
                        }

                        bidsContext.UpdateRange(unsyncedBids);
                        await bidsContext.SaveChangesAsync(stoppingToken);

                        _logger.LogWarning("End bids synchronization.");
                    }

                    await Task.Delay(_options.BackgroundTaskExecutingTimeInSeconds > 0 ? _options.BackgroundTaskExecutingTimeInSeconds * 3000 : 180000, stoppingToken);
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();

                    GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
                    GC.Collect();
                }
            }
            catch(Exception ex)
            {
                _logger.LogError($"An error has occurred when trying to synchronize bids. {ex}");
                Debug.WriteLine(ex);
            }

            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
                GC.Collect();
            }
        }
    }
}
