﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.ViewModels.Tenders
{
    public class TenderViewModel
    {
        public int UnicalNumber { get; set; }
        public int LotNumber { get; set; }
        public string Number { get; set; }
        public DateTime EndTime { get; set; }
        public string TypeName { get; set; }
        public string CityFrom { get; set; }
        public string CityTo { get; set; }
        public string CountryFrom { get; set; }
        public string CountryTo { get; set; }
        public string CargoType { get; set; }
        public string TransportType { get; set; }
        public DateTime LoadingFrom { get; set; }
        public DateTime LoadingTo { get; set; }
        public DateTime UnloadingDate { get; set; }
        public string CustomerComment { get; set; }
        public string CountryFromISO { get; set; }
        public string CountryToISO { get; set; }
        public string currency { get; set; }

        public string TenderType { get; set; }

        public int? TenderBackUnicalNumber { get; set; }

        public long LoadingFromInMs { get; set; }

        public long UnloadingInMs { get; set; }
    }
}
