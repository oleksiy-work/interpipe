﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.ViewModels.Tenders
{
    public class TenderGroupViewModel
    {
        public string Country { get; set; }

        public IEnumerable<TenderViewModel> Items { get; set; }
    }
}
