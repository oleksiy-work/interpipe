﻿namespace Interpipe.Tender.ViewModels.Bids
{
    public class MakeBidViewModel
    {
        public int UnicalNumber { get; set; }

        public int LotNumber { get; set; }

        public decimal Price { get; set; }

        public string Comment { get; set; }

        public string Currency { get; set; }

        public int? VehiclesAmount { get; set; }
    }
}
