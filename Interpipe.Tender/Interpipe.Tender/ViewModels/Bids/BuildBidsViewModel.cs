﻿namespace Interpipe.Tender.ViewModels
{
    public class BuildBidsViewModel
    {
        public int UnicalNumber { get; set; }

        public int LotNumber { get; set; }

        public int? TenderBackUnicalNumber { get; set; }
    }
}
