﻿using System;

namespace Interpipe.Tender.ViewModels.Bids
{
    public class BidInformationViewModel
    {
        public int UnicalNumber { get; set; }

        public int LotNumber { get; set; }

        public string Number { get; set; }

        public string CountryTo { get; set; }

        public string City { get; set; }

        public DateTime LoadingFrom { get; set; }

        public DateTime LoadingTo { get; set; }

        public DateTime UnloadDate { get; set; }

        public DateTime TenderEndTime { get; set; }

        public string Currency { get; set; }

        public decimal? MyPrice { get; set; }

        public decimal CurrenPrice { get; set; }

        public string PaymentConditions { get; set; }

        public string Comment { get; set; }

        public decimal? PriceStep { get; set; }

        public string CargoType { get; set; }

        public string TransportType { get; set; }

        public string AdditionalInfo { get; set; }

        public string FilesUrl { get; set; }

        public string CityFrom { get; set; }

        public bool IsReduction { get; set; }

        public double CloseAfter { get; set; }

        public decimal Price { get; internal set; }

        public string CountryFrom { get; set; }

        public string PaymentPostponement { get; set; }

        public string Status { get; set; }

        public int? TenderBackUnicalNumber { get; set; }

        public int CarsAmount { get; set; } = 1;

        public int MyCarsAmount { get; set; }

        public string CountryFromISO { get; set; }
        public string CountryToISO { get; set; }
        public string CityTo { get; set; }

        public string TenderType { get; set; }
        public long LoadingFromInMs { get; internal set; }
        public long UnloadingInMs { get; internal set; }

        public bool IsFinished { get; set; }

        public DateTime EndTimeUtc { get; set; }

        public DateTime ServerTime { get; set; }
    }
}
