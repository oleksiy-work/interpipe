import {Component, OnDestroy} from '@angular/core';
import { SignalrService } from './services/signalr.service';
import {SignalRService} from "./services/signal.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnDestroy {
  title = 'app';
  constructor(
    // private signalTimer: SignalrService,
    private signalRService: SignalRService
  ) {
    /*this.signalTimer.startConnection().then(() => {
      this.signalTimer.addListener();
    })*/
    this.signalRService.connect();
  }
  ngOnDestroy() {
    this.signalRService.disconnect();
  }
}
