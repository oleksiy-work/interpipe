import {Injectable, Inject, OnDestroy} from '@angular/core';
import { TenderGroup } from '../models/tenders/tender';
import { HttpClient } from '@angular/common/http';
import { TendersResponse } from '../models/responses/tenders.response';
import { BidsInfoRequest } from '../models/request/bids/bids.reques.info';
import { TenderBid } from '../models/tenders/tender.bid';
import { MakeBidReques } from '../models/request/bids/make.bid.reques';
import { BidsResponse } from '../models/responses/bids.response';
import { Observable, Subject, Subscription } from 'rxjs';
import { TimerService } from './timer.service';


@Injectable()
export class ApiService implements OnDestroy {

    private apiPath: string;

    private seconds: number = 60;

    private udpateTimerSubscription: Subscription;
    private updateDateSubscription: Subscription;

    private _serverDateChanged = new Subject<Date>();
    _serverDateChanged$ = this._serverDateChanged.asObservable();

    constructor(@Inject('BASE_URL') baseUrl, private http: HttpClient, private timer: TimerService ) {
      this.apiPath = `${baseUrl}api/`;

      this.udpateTimerSubscription = this.timer.timer.subscribe(x => {
        if (++this.seconds >= 60) {
          this.updateDateSubscription = this.getUtcDate().subscribe(x => {
            this.seconds = 0;
            this._serverDateChanged.next(new Date(x));
          });
        }
        else {
          if (this.updateDateSubscription) {
            this.updateDateSubscription.unsubscribe();
            this.updateDateSubscription = undefined;
          }
        }
      });

    }
    ngOnDestroy() {
      this.udpateTimerSubscription.unsubscribe();
    }
  private tenders = 'tenders/';
    private bids = 'bids/';
    private auth = "auth/"


    public getTenders(): Promise<TendersResponse> {
        return this.http.get<TendersResponse>(this.apiPath + this.tenders).toPromise();
    }

    public getNewTendersAmount(): Promise<number> {
        return this.http.get<number>(this.apiPath + this.tenders + 'new/').toPromise();
    }

    public buildBidsInfo(bidsData: BidsInfoRequest[]): Promise<BidsResponse> {
        return this.http.post<BidsResponse>(this.apiPath + this.bids + 'selected-bids-info', bidsData).toPromise();
    }

    public makeBids(data: MakeBidReques[]): Promise<Array<TenderBid>> {
        return this.http.post<Array<TenderBid>>(this.apiPath + this.bids, data).toPromise();
    }

    public isAuthenticated(): Promise<boolean> {
        return this.http.get<boolean>(this.apiPath + this.auth + 'is-authenticated')
            .toPromise();
    }

    public loginWithEmail(email: string) {
        return this.http.post<any>(this.apiPath + this.auth + 'with-email/', { email }).toPromise();
    }

    public logOut(){
        return this.http.post<any>(this.apiPath + this.auth,{}).toPromise();
    }

    public getUtcDate(): Observable<Date> {
      return this.http.get<Date>(this.apiPath + this.tenders + 'utcdate/');
    }
}
