import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  @Output() onSignalRMessage: EventEmitter<any> = new EventEmitter();
  private hubConnection: any = signalR.HubConnection;

  constructor(private http: HttpClient) {
    // this.startConnection();
  }


  public startConnection(): Promise<any> {
    let cdx = this;
    Object.defineProperty(WebSocket, 'OPEN', { value: 1, });
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(window.location.origin + '/tendertimehub')
      .build();

    return new Promise((resolve, reject) => {
      this.hubConnection
        .start()
        .then(() => {
          console.log('Timer Hub Connection started');
          this.invoke();
          // this.addListener();

          resolve(true);
        })
        .catch((err: string) => {
          console.log('Timer Hub Error while starting connection: ' + err);
          setTimeout(function () {
            cdx.startConnection();
          }, 1000);
        })
    });
  }

  public invoke = () => {
    this.hubConnection.invoke('GetUtcDate')
      .catch((error: any) => {
        console.log(`error: ${error}`);
      });
  }

  public addListener = () => {
    this.hubConnection.on('GetTenderTime', (data: any) => {
      //console.log(`GetTenderTime: ${data}`);
      this.newMessage(data as any);
    }, error => {
        console.log(error);
    });
  }

  private newMessage(data: any) {
    this.onSignalRMessage.emit(data);
  }
}

