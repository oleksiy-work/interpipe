import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";

@Injectable()
export class LocalizationService {
  private apiPath: string;

  constructor(@Inject('BASE_URL') baseUrl, private http: HttpClient) {
    this.apiPath = `${baseUrl}api/`;
  }

  public setLang(lang: string) {
    return this.http.post<any>(this.apiPath + `Localization/${lang}`, {}).toPromise();
  }

}
