import { Injectable } from '@angular/core';
import { time } from 'console';
import { Observable, timer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  private _timer: Observable<number>;

  constructor() {
  }

  get timer() {
    if (!this._timer) {
      this._timer = timer(1000, 1000);
    }
    return this._timer;
  }
}
