import {EventEmitter, Injectable, Output} from "@angular/core";
import {HubConnection, HubConnectionBuilder} from "@aspnet/signalr";
import {HttpClient} from "@angular/common/http";
import {from, Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private hubConnection: HubConnection;
  private message$: Subject<any> = new Subject<any>();
  constructor(private http: HttpClient) {}
  public connect() {
    this.startConnection();
    this.addListeners();
  }
  public sendMessageToHub() {
    return this.hubConnection.invoke('GetUtcDate')
      .then(() => { console.log('message sent successfully to hub'); })
      .catch((err) => console.log('error while sending a message to hub: ' + err));
  }
  private getConnection(): HubConnection {
    console.log('get connection');
    return new HubConnectionBuilder()
      .withUrl(window.location.origin + '/tendertimehub')
      //.withAutomaticReconnect()
      .build();
  }
  private startConnection() {
    this.hubConnection = this.getConnection();
    this.hubConnection.start()
      .then(() => {
        console.log('connection were started now');
        this.sendMessageToHub();
      })
      .catch((err) => {
        console.log('error while establishing signalr connection: ' + err);
        setTimeout(() => {
          this.startConnection();
        }, 1000);
      });
  }
  private addListeners() {
    this.hubConnection.on('GetTenderTime', (data) => {
      this.message$.next(data);
    });
  }
  public disconnect() {
    this.hubConnection.stop();
  }
  public getMessage(): Observable<any> {
    return this.message$.asObservable();
  }
}
