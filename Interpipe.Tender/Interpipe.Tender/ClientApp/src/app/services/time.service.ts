import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  public _timer: Observable<number> = timer(1000, 1000).pipe(tap(time => console.log('Timer Init', time)));

  constructor() {}
}
