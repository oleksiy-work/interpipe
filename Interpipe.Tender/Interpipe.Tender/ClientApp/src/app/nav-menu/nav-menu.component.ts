import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { interval, Subscription, timer } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiService } from '../services/api.service';
import { LocalizationService } from '../services/localization.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
})
export class NavMenuComponent implements OnInit, OnDestroy {

  //email: string = 'Illyana.Maetnaya@interpipe.biz';
  email: string = 'xss@it-dnepr.it.ua';

  constructor(private api: ApiService, private localizationService: LocalizationService, private translate: TranslateService) { }

  private isAuthenticated: boolean;
  private _newTendersAmount: number = 0;;
  private _updatesTimerSubscription: Subscription;
  private _updatesTimer;
  async ngOnInit() {
    this.isAuthenticated = await this.api.isAuthenticated();

    this.startUpdatesTimer();
  }

  private startUpdatesTimer() {
    if (this.IsAuthenticated) {
      const updatesTime = timer(30000, 30000);

      this._updatesTimerSubscription = updatesTime.subscribe(async x => {
        await this.getNewTendersAmount();
      });
    }
  }

  get IsAuthenticated() {
    return this.isAuthenticated;
  }

  get mainUrl() {
    return environment.mainSiteUrl;
  }

  async processLogin(event) {
    event.preventDefault();

    await this.api.loginWithEmail(this.email);

    location.reload();
  }

  get newTendersAmount() {
    return this._newTendersAmount;
  }

  async getNewTendersAmount() {
    if (!this.isAuthenticated)
      return 0;

    this._newTendersAmount = await this.api.getNewTendersAmount();
  }

  resetTendersAmount() {
    this._newTendersAmount = 0;
    this.startUpdatesTimer();
  }

  async logOut(event) {
    event.preventDefault();

    await this.api.logOut();

    location.reload();
  }

  localization(lang: string) {
    console.log(lang);
    this.translate.use(lang);
    this.localizationService.setLang(lang.toUpperCase());
  }

  ngOnDestroy(): void {
    if (this._updatesTimerSubscription) {
      this._updatesTimerSubscription.unsubscribe();
      this._updatesTimerSubscription = null;
    }
  }

}
