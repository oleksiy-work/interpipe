import { Tender } from "../models/tenders/tender";
import { TenderDirections } from "../models/direcitons/tender.directions";
import { TenderBase } from "../models/tenders/tender.base";

export class TenderHelper {
    public static getDirection(tender: TenderBase): TenderDirections {

        if (tender.countryFromISO == "UA" && tender.countryToISO != "UA")
            return TenderDirections.export;
        if (tender.countryFromISO != "UA" && tender.countryToISO == "UA")
            return TenderDirections.import;
        if (tender.countryFromISO == "UA" && tender.countryToISO == "UA")
            return TenderDirections.internal;
    }
}