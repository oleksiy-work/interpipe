import { TenderGroup, Tender } from "../models/tenders/tender";
import { FiltersHelperBase } from "./filters.helper.base";




export class FiltersHelper extends FiltersHelperBase {
    private _sourceTenders: TenderGroup[];

    constructor(tenders: TenderGroup[]) {
        super();
        this._sourceTenders = tenders;
    }


    onFiltersUpdated() {
        this._filteredTenders.length = 0;
        this._tenderGroups = null;
    }

    getFilteredTenders(): Array<Tender> {
        return this.filteredTenders;
    }

    getAllTenders(): Array<Tender> {
        let result = new Array<Tender>();
        this._sourceTenders
            .filter(x => x.items.length > 0)
            .forEach(x => {
                x.items.forEach(f => result.push(f));
            });

        return result;
    }


    private _filteredTenders: Tender[] = new Array<Tender>();

    get filteredTenders(): Tender[] {
        if (this._filteredTenders && this._filteredTenders.length > 0)
            return this._filteredTenders;



        this._filteredTenders.length = 0;

        let tenderGroups = this.filterTenderGroups();

        if (tenderGroups) {
            tenderGroups
                .filter(x => x.items.length > 0)
                .forEach(x => {
                    x.items.forEach(f => this._filteredTenders.push(f));
                });
        }

        return this._filteredTenders;

    }

    get tenderGroups() {

        return this.filterTenderGroups();
    }



    private _tenderGroups: TenderGroup[];
    private filterTenderGroups(): TenderGroup[] {
        if (!this._tenderGroups) {
            let result = Array<TenderGroup>();

            this._sourceTenders.forEach(country => {
                let items = country.items.filter(f => {
                    return this._filters.checkTenderCondition(f);
                });
                this.addToTendersArray(result, items);
            });


            this._tenderGroups = result;
        }

        return this._tenderGroups;
    }
}
