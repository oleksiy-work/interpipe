export class ArrayHelper {
    public static filterUnique(array: any[]): any[] {
        let result = new Array();

        array.forEach(x => {
            if (!result.some(s => s == x)) {
                result.push(x);
            }
        });

        return result;
    }
}