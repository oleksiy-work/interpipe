export class DatetimeHelper {

    public static toDateString(date: Date): string {

        if (typeof (date) != typeof (Date))
            date = new Date(date);

        let day = date.getDate().toString().padStart(2, '0');
        let month = (date.getMonth() + 1).toString().padStart(2, '0');
        let year = date.getFullYear().toString();

        return `${day}.${month}.${year}`;
    }

    public static toShortMonth(date: Date) {
        if (typeof (date) != typeof (Date))
            date = new Date(date);

        return date.toLocaleDateString("ru-RU", { day: "numeric", month: "short" });
    }

    public static toShortTime(date: Date) {

        if (typeof (date) != typeof (Date))
            date = new Date(date);
        
        let result = `${DatetimeHelper.padStart(date.getHours())}:${DatetimeHelper.padStart(date.getMinutes())}`;
        return result;
    }

    private static padStart(obj, digits: number = 2, fill: string = '0') {
        return obj.toString().padStart(digits, fill);
    }

}