import { TenderGroup, Tender } from "../models/tenders/tender";
import { TenderFilter } from "../models/filters/tender.filter";
import { TenderBase } from "../models/tenders/tender.base";
import { TenderHelper } from "./tender.helper";
import { TenderDirections } from "../models/direcitons/tender.directions";


export abstract class FiltersHelperBase {

    protected _filters: FiltersModel = new FiltersModel();

    public updateFilters(filters: FiltersModel) {
        this.onFiltersUpdated();
        this._tenderFilter = null;
        this._filters = new FiltersModel(filters);
    }

    private _tenderFilter: TenderFilter;
    get filterOptions(): TenderFilter {
        if (this._tenderFilter)
            return this._tenderFilter;

        let items = this.getAllTenders();
        
        this._tenderFilter = {
            countriesFrom: this.onlyUniqueObjects(items
                .filter(x => this._filters.filterCountryFromDependOnDirection(x))
                .map(x => {
                    return {
                        name: x.countryFrom,
                        iso: x.countryFromISO
                    }
                })),
            countriesTo: this.onlyUniqueObjects(
                items
                    .filter(x => {
                        return this._filters.filterCountryTo(x) && this._filters.filterCountryToByCity(x) && this._filters.filterCountryFromDependOnDirection(x);
                    })
                    .map(x => {
                        return {
                            name: x.countryTo,
                            iso: x.countryToISO
                        }
                    })),
            citiesFrom: this.onlyUniqueObjects(items
                .filter(x => this._filters.filterCountryFromDependOnDirection(x))
                .map(x => {
                    return {
                        name: x.cityFrom,
                        countryIso: x.countryFromISO
                    }
                })),
            citiesTo: this.onlyUniqueObjects(
                items
                    .filter(x => {
                        return this._filters.filterCountryFromDependOnDirection(x) && this._filters.filterCityTo(x) && this._filters.filterCityToByCountry(x);
                    })
                    .map(x => {
                        return {
                            name: x.cityTo,
                            iso: x.countryToISO
                        }
                    })),
            currencies: this.getAllTenders().map(x => x.currency).filter(this.onlyUnique)
        }

        return this._tenderFilter;
    }


    protected abstract getFilteredTenders(): Array<TenderBase>
    protected abstract getAllTenders(): Array<TenderBase>
    protected abstract onFiltersUpdated();

    protected onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    protected onlyUniqueObjects(array) {
        const result = new Array();
        array.forEach(element => {
            if (!result.some(s => s.name === element.name)) {
                result.push(element);
            }
        });

        return result;
    }

    get filterState() {
        return this._filters.filtersStarte;
    }


    protected addToTendersArray(tendersArray: TenderGroup[], tenders: Tender[]) {

        tenders.forEach(tender => {
            let c = tendersArray.filter(x => x.country == tender.countryTo);

            if (c.length > 0) {
                c[0].items = [...c[0].items, tender];
            }
            else {
                tendersArray.push({ country: tender.countryTo, items: [tender] });
            }
        });
    }

}




export class FiltersModel {
    countryTo = [];
    countryFrom = [];
    cityFrom = [];
    cityTo = [];
    export = false;
    import = false;
    internal = false;
    unloadDate: Date = null;
    once = false;
    circle = false;
    group = false;
    contract = false;
    dates = null;
    currency = [];

    startDateMilliseconds: number = 0;
    endDateMilliseconds: number = 0;

    constructor(model?: any) {

        if (model) {
            this.countryTo = model.countryTo;
            this.countryFrom = model.countryFrom;
            this.cityFrom = model.cityFrom;
            this.cityTo = model.cityTo;
            this.export = model.export;
            this.import = model.import;
            this.internal = model.internal;
            this.unloadDate = model.unloadDate;
            this.once = model.once;
            this.circle = model.circle;
            this.group = model.group;
            this.contract = model.contract;
            this.dates = model.dates;
            this.currency = model.currency;

            if (this.dates) {
                if (this.dates.begin) {
                    let beginDate = this.dates.begin as Date;
                    this.startDateMilliseconds = Date.UTC(beginDate.getFullYear(), beginDate.getMonth(), beginDate.getDate()).valueOf();
                }

                if (this.dates.end) {
                    let endDate = this.dates.end as Date;
                    this.endDateMilliseconds = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()).valueOf();
                }
            }
        }
    }

    public get filtersStarte() {
        let state = {
            countryTo: !this.isNullOrEmpty(this.countryTo),
            countryFrom: !this.isNullOrEmpty(this.countryFrom),
            cityFrom: !this.isNullOrEmpty(this.cityFrom),
            cityTo: !this.isNullOrEmpty(this.cityTo),
            export: !this.isNullOrEmpty(this.export),
            import: !this.isNullOrEmpty(this.import),
            internal: !this.isNullOrEmpty(this.internal),
            unloadDate: !this.isNullOrEmpty(this.unloadDate),
            once: !this.isNullOrEmpty(this.once),
            circle: !this.isNullOrEmpty(this.circle),
            group: !this.isNullOrEmpty(this.group),
            contract: !this.isNullOrEmpty(this.contract),
            dates: !this.isNullOrEmpty(this.dates),
            currency: !this.isNullOrEmpty(this.currency),
        };

        return state;
    }

    get needToCheckTendersDirection() {
        return this.import || this.export || this.internal;
    }

    get isExport(): boolean {
        return this.isPropertyTrue(this.export);
    }

    get isImport(): boolean {
        return this.isPropertyTrue(this.import);
    }

    get isInternal(): boolean {
        return this.isPropertyTrue(this.internal);
    }

    isExportTender(tender: TenderBase): boolean {
        return TenderHelper.getDirection(tender) == TenderDirections.export;
    }

    isImportTender(tender: TenderBase): boolean {
        return TenderHelper.getDirection(tender) == TenderDirections.import;
    }

    isInternalTender(tender: TenderBase): boolean {
        return TenderHelper.getDirection(tender) == TenderDirections.internal;
    }

    public checkTenderCondition(tender: TenderBase): boolean {

        if (this.needToCheckTendersDirection) {

            let imp = false;
            let exp = false;
            let int = false;



            if (this.export && this.isExportTender(tender))
                exp = true;

            if (this.import && this.isImportTender(tender))
                imp = true;

            if (this.internal && this.isInternalTender(tender))
                int = true;

            if (!(exp || imp || int))
                return false;
        }

        return (
            this.filterTenderDates(tender)
            && (this.isNullOrEmpty(this.countryFrom) || this.countryFrom.some(x => x == tender.countryFromISO))
            && (this.isNullOrEmpty(this.countryTo) || this.countryTo.some(x => x == tender.countryToISO))
            && (this.isNullOrEmpty(this.cityFrom) || this.cityFrom.some(x => x == tender.cityFrom))
            && (this.isNullOrEmpty(this.cityTo) || this.cityTo.some(x => x == tender.cityTo))
            && (this.isNullOrEmpty(this.unloadDate) || this.compareDate(this.unloadDate, new Date(tender.unloadingDate)))
            && (this.isNullOrEmpty(this.currency) || this.currency.some(x => x == tender.currency))

            && this.filterType(tender)
        )
    }

    private filterType(tender) {

        if (this.isNullOrEmpty(this.once)
            && this.isNullOrEmpty(this.circle)
            && this.isNullOrEmpty(this.group)
            && this.isNullOrEmpty(this.contract)) {
            return true;
        }

        let type = tender.tenderType;

        //once
        let once = !this.isNullOrEmpty(this.once) && type.trim() == '';
        //circle
        let circle = !this.isNullOrEmpty(this.circle) && type.trim() == 'RNG';
        //group
        let group = !this.isNullOrEmpty(this.group) && type.trim() == 'GRP';
        //contact
        let contract = !this.isNullOrEmpty(this.contract) && type.trim() == 'CON';

        return (once || circle || group || contract);
    }

    public compareDate(first: Date, second: Date) {
        return first.getMonth() == second.getMonth() && first.getDate() == second.getDate();
    }

    public filterTenderDates(tender: TenderBase) {

        if (this.startDateMilliseconds == 0 && this.endDateMilliseconds == 0)
            return true;

        return tender.loadingFromInMs >= this.startDateMilliseconds && tender.loadingFromInMs <= this.endDateMilliseconds;
    }


    public IsEmpty(): boolean {

        return this.isNullOrEmpty(this.countryTo)
            && this.isNullOrEmpty(this.countryFrom)
            && this.isNullOrEmpty(this.cityFrom)
            && this.isNullOrEmpty(this.cityTo)
            && this.isNullOrEmpty(this.export)
            && this.isNullOrEmpty(this.import)
            && this.isNullOrEmpty(this.internal)
            && this.isNullOrEmpty(this.unloadDate)
            && this.isNullOrEmpty(this.once)
            && this.isNullOrEmpty(this.circle)
            && this.isNullOrEmpty(this.group)
            && this.isNullOrEmpty(this.contract)
            && this.isNullOrEmpty(this.dates)
            && this.isNullOrEmpty(this.currency);

    }

    public filterCountryTo(tender: TenderBase) {
        if (this.isNullOrEmpty(this.countryFrom))
            return true;

        return this.countryFrom.some(s => s == tender.countryFromISO);
    }

    public filterCountryToByCity(tender: TenderBase) {
        if (this.isNullOrEmpty(this.cityFrom))
            return true;

        return this.cityFrom.some(s => s == tender.cityFrom);
    }

    public filterCityTo(tender: TenderBase) {
        if (this.isNullOrEmpty(this.cityFrom))
            return true;

        return this.cityFrom.some(s => s == tender.cityFrom);
    }

    public filterCityToByCountry(tender: TenderBase) {
        if (this.isNullOrEmpty(this.countryTo))
            return true;

        return this.countryTo.some(x => x == tender.countryToISO);
    }

    public filterCountryFromDependOnDirection(tender) {
        let dir = TenderHelper.getDirection(tender);

        if (this.isNullOrEmpty(this.export) && this.isNullOrEmpty(this.import) && this.isNullOrEmpty(this.internal))
            return true;

        return (dir == TenderDirections.export && this.export)
            || (dir == TenderDirections.import && this.import)
            || (dir == TenderDirections.internal && this.internal);
    }

    private isPropertyTrue(prop): boolean {
        return !this.isNullOrEmpty(prop) && prop === true;
    }

    private isNullOrEmpty(prop) {
        return prop == null || prop.length == 0 || prop == false;
    }
}