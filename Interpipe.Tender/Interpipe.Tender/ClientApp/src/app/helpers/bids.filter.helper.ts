import { FiltersHelperBase } from "./filters.helper.base";

import { TenderBase } from '../models/tenders/tender.base';
import { TenderBid } from "../models/tenders/tender.bid";
import { Console } from "console";

export class BidsFilterHelper extends FiltersHelperBase {

    private _sourceBids: TenderBid[];
    private _filteredBids: TenderBid[];
    private _tab = 1;
    constructor(bids: TenderBid[]) {
        super();
        this._sourceBids = bids;
        this._filteredBids = new Array<TenderBid>();


    }

    protected onFiltersUpdated() {
        this._filteredBids.length = 0;
        this._simple = null;
        this._group = null;
        this._circle = null;
        this._contract = null;
    }

    protected getFilteredTenders(): TenderBid[] {
        return this.filteredTenderBids;
    }
    protected getAllTenders(): TenderBid[] {
        if (this._tab > 1)
            return this._sourceBids.filter(x => x.tenderType == this.tabType(this._tab));

        return this._sourceBids;
    }

    changeTab(tab) {
        this._tab = tab;
    }


    private _simple: TenderBid[];
    get simple() {
        if (!this._simple)
            this._simple = this.filteredTenderBids.filter(x => x.tenderType.trim() == '');

        return this._simple;
    }

    private _circle: TenderBid[];
    get circle() {
        if (!this._circle)
            this._circle = this.filteredTenderBids.filter(x => x.tenderType == 'RNG');

        return this._circle;
    }

    private _group: TenderBid[];
    get group() {
        if (!this._group)
            this._group = this.filteredTenderBids.filter(x => x.tenderType == 'GRP');

        return this._group;
    }

    private _contract: TenderBid[];
    get contract() {
        if (!this._contract)
            this._contract = this.filteredTenderBids.filter(x => x.tenderType == 'CON');

        return this._contract;
    }

    private tabsCounter = {}
    getTabTendersAmount(tabIndex: number, current: boolean = false) {
        if (!this._sourceBids)
            return 0;

        if (current)
            return this.filteredTenderBids.filter(x => x.tenderType.trim() == this.tabType(tabIndex)).length;

        if (!this.tabsCounter[tabIndex])
            this.tabsCounter[tabIndex] = this._sourceBids.filter(x => x.tenderType.trim() == this.tabType(tabIndex)).length;

        return this.tabsCounter[tabIndex];
    }

    private tabType(tab: number) {
        switch (tab) {
            case 1:
                return '';
            case 2:
                return 'RNG';
            case 3:
                return 'GRP'
            case 4:
                return 'CON'
        }
    }


    get filteredTenderBids() {

        if (this._filteredBids && this._filteredBids.length > 0)
            return this._filteredBids;


        if (this._filters.IsEmpty())
            return this._sourceBids;

        this._filteredBids = this._sourceBids.filter(x => this._filters.checkTenderCondition(x));


        return this._filteredBids;
    }

}