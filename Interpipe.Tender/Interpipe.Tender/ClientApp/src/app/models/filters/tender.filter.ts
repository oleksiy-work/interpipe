export interface TenderFilter {
    countriesFrom: CountryFilterInfo[];
    countriesTo: CountryFilterInfo[];
    citiesFrom: CityFilterInfo[];
    citiesTo: CityFilterInfo[];
    currencies : string[];
}

export interface CountryFilterInfo{
    name : string;
    iso : string;
}

export interface CityFilterInfo{
    name : string;
    countryIso : string;
}