import { TenderBase } from "./tender.base";

export interface Tender extends TenderBase {
    unicalNumber: number;
    lotNumber: number;
    number: number;
    endTime: Date;
    typeName: string;
    isReduction: number;
    currency: string;
    minPrice: number;
    priceStep?: number;
    ownerName: string;
    cityFrom: string;
    cityTo: string;
    countryFrom: string;
    countryTo: string;
    cargoType: string;
    transportType: string;
    loadingFrom: Date;
    loadingTo: Date;
    unloadingDate: Date;
    isBooking: number;
    custom: string;
    customerComment: string;
    warehouseName: string;
    warehouseAddress: string;
    managerName: string;
    managerPhone: string;
    managerEmail: string;
    canBid: number;
    countryFromISO: string;
    countryToISO: string;
    tenderBackUnicalNumber?: number;
    loadingFromInMs: number;
    unloadingInMs: number;
}

export interface TenderGroup {
    country: string;
    items: Array<Tender>;
}