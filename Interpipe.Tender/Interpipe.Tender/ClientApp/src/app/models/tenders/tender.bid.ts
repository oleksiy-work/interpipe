import { TenderBase } from "./tender.base";

export interface TenderBid extends TenderBase {
    additionalInfo: string;
    cargoType: string;
    comment: string;
    countryTo: string;
    currenPrice: number;
    currency: string;
    filesUrl: string;
    loadingFrom: Date;
    loadingTo: Date;
    lotNumber: number;
    myPrice: number;
    paymentConditions: string
    priceStep: number;
    tenderEndTime: Date;
    transportType: string;
    unicalNumber: number;
    unloadDate: Date;
    cityFrom: string;
    isReduction: boolean;
    closeAfter: number;
    price: number;
    number: string;
    countryFrom: string;
    paymentPostponement: string;
    status: string;
    tenderBackUnicalNumber?: number;
    carsAmount: number;
    myCarsAmount?: number;
    countryFromISO: string;
    countryToISO: string;
    cityTo: string;
    tenderType: string;
    loadingFromInMs: number;
    unloadingInMs: number;
    isFinished: boolean;
    failedToSubmit: boolean;
  endTimeUtc: number;
  serverTime: number;
}
