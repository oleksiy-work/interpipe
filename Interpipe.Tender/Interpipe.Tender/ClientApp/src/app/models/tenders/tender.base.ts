export interface TenderBase {
    countryTo: string;
    currency: string;
    cityFrom: string;
    countryFrom: string;
    countryFromISO: string;
    countryToISO: string;
    cityTo: string;
    tenderType: string;
    loadingFromInMs: number;
    unloadingInMs: number;
    isFinished: boolean;
    unloadingDate: Date;
}