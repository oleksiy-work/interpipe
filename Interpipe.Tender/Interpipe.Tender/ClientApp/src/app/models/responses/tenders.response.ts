import { TenderGroup } from "../tenders/tender";
import { TenderFilter } from "../filters/tender.filter";

export interface TendersResponse {
    tenders: Array<TenderGroup>;
    filters: TenderFilter;
}