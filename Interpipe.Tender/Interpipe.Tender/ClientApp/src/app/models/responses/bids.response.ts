import { TenderBid } from "../tenders/tender.bid";
import { TenderFilter } from "../filters/tender.filter";

export interface BidsResponse {
    bids: Array<TenderBid>;
    filters: TenderFilter;
}
