export interface MakeBidReques {
    unicalNumber: number;

    lotNumber: number;

    price: number;

    comment: string;

    currency: string;

    vehiclesAmount?: number;
}