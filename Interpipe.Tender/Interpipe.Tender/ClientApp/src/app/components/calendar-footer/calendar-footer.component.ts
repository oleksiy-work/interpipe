import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SatCalendarFooter, SatCalendar, SatDatepicker, DateAdapter } from 'saturn-datepicker';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-calendar-footer',
  templateUrl: './calendar-footer.component.html',
  styleUrls: ['./calendar-footer.component.scss']
})
export class CalendarFooterComponent<Date> implements OnInit, SatCalendarFooter<Date> {

  private destroyed = new Subject<void>();

  constructor(private calendar: SatCalendar<Date>,
    private datePicker: SatDatepicker<Date>,
    private dateAdapter: DateAdapter<Date>,
    cdr: ChangeDetectorRef) {

    calendar.stateChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => { cdr.markForCheck() });

  }

  ngOnInit() {
  }

  clear() {
    
    this.datePicker._selectRange(
      {
        begin: null, end: null
      });

      this.calendar._dateSelected(null);
      this.datePicker.close();
      

  }

}
