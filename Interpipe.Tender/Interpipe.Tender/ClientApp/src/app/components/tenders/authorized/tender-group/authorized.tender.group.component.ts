import { Component, OnInit, Input } from '@angular/core';
import { BaseTenderGroupComponent } from 'src/app/components/tenders/base/tender.group.base';
import { TenderDirections } from 'src/app/models/direcitons/tender.directions';
import { TenderHelper } from 'src/app/helpers/tender.helper';

@Component({
    selector: '[app-authorized-tender-group]',
    templateUrl: './authorized.tender.group.component.html'
})
export class AuthorizedTenderGroupComponent extends BaseTenderGroupComponent {

    get rowText() {

        switch (TenderHelper.getDirection(this.tender)) {
            case TenderDirections.export:
                return 'Международные перевозки (Экпорт)';
            case TenderDirections.import:
                return 'Международные перевозки (Импорт)';
            case TenderDirections.internal:
                return 'Внутренние перевозки по Украине';
        }
        return '';
    }

    get rowClass() {
        switch (TenderHelper.getDirection(this.tender)) {
            case TenderDirections.export:
                return 'bg-orange';
            case TenderDirections.import:
                return 'bg-green';
            case TenderDirections.internal:
                return 'bg-blue';
        };
        return '';
    }

    private isSelected: boolean;
    get IsSelected() {
        return this.isSelected;
    }

    set IsSelected(selected) {
        this.isSelected = selected;
    }

    toggleSelection(event) {

        this.isSelected = event.target.checked;
    }
}