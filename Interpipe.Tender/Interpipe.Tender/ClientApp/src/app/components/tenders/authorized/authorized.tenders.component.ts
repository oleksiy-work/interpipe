import { Component, ViewChildren, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BaseTendersComponent } from '../base/tender.base';
import { AuthorizedTenderGroupComponent } from './tender-group/authorized.tender.group.component';
import { Router } from '@angular/router';
import { BidsInfoRequest } from 'src/app/models/request/bids/bids.reques.info';
import { DateAdapter } from 'saturn-datepicker';
import { MatDatepicker } from '@angular/material';

@Component({
  selector: 'app-tender-auth',
  templateUrl: './authorized.tenders.component.html',
  host: {
    '(window:scroll)': 'onScroll($event)',
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthorizedTendersComponent extends BaseTendersComponent {

  constructor(api: ApiService,
    formBuilder: FormBuilder,
    private router: Router,
    private dateAdapter: DateAdapter<Date>,
    cd: ChangeDetectorRef) {

    super(api, formBuilder, cd);

    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }

    this.dateAdapter.setLocale("ru-RU");

  }

  @ViewChildren(AuthorizedTenderGroupComponent) tenderGroups: Array<AuthorizedTenderGroupComponent>;

  loadData() {
    return this.api.getTenders();
  }

  get checkedRowsAmount() {
    if (this.tenderGroups)
      return this.tenderGroups.filter(
        x => x.IsSelected && this.tendersList.some(
          s => s.unicalNumber == x.tender.unicalNumber
            && s.lotNumber == x.tender.lotNumber))
        .length;

    return 0;
  }

  setBids(event) {
    event.preventDefault();

    let data: Array<BidsInfoRequest> = this.tenderGroups.filter(x => x.IsSelected).map(x => {
      return {
        lotNumber: x.tender.lotNumber,
        unicalNumber: x.tender.unicalNumber,
        tenderBackUnicalNumber: x.tender.tenderBackUnicalNumber
      } as BidsInfoRequest;
    });


    this.router.navigate(['bids/one-time'], {
      state: data
    });

  }

  private _allSelected = false;

  get allSelected() {
    return this._allSelected;
  }

  selectAll(event) {
    this._allSelected = event.srcElement.checked;
    this.updateSelection();

  }

  private updateSelection() {
    this.tenderGroups.forEach(x => { x.IsSelected = this.allSelected; });
  }
  @ViewChild('picker', { static: false }) datePicker: MatDatepicker<Date>;
  onScroll(event) {
    if (this.datePicker && this.datePicker.opened)
      this.datePicker.close();
  }
}
