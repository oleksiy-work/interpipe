import { Component, OnInit, Input } from '@angular/core';
import { Tender } from 'src/app/models/tenders/tender';
import { DatetimeHelper } from 'src/app/helpers/dateTime.helper';
import { BaseTenderGroupComponent } from 'src/app/components/tenders/base/tender.group.base';

@Component({
    selector: '[app-unauthorized-tender-group]',
    templateUrl: './unauthorized.tender.group.component.html'
})
export class UnauthorizedTenderGroupComponent extends BaseTenderGroupComponent {


}