import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { TenderGroup, Tender } from 'src/app/models/tenders/tender';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TenderFilter } from 'src/app/models/filters/tender.filter';
import { BaseTendersComponent } from '../base/tender.base';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-tender-un-auth',
  templateUrl: './unauthorized.tenders.component.html',
})
export class UnauthorizedTendersComponent extends BaseTendersComponent {


  loadData() {
    return this.api.getTenders();
  }

  constructor(api: ApiService,
    formBuilder: FormBuilder,
    private dateAdapter: DateAdapter<Date>,
    cd: ChangeDetectorRef) {

    super(api, formBuilder, cd);

    this.dateAdapter.getFirstDayOfWeek = () => { return 1; }
    this.dateAdapter.setLocale("ru-RU");

    this.selectCalendarRange = false;

  }

  dateInput(event) {
    
  }

}
