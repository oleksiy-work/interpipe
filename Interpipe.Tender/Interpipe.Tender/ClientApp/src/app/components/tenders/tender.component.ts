import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
    selector: 'app-tenders',
    templateUrl: 'tender.component.html'
})
export class TenderComponent implements OnInit {

    constructor(private api: ApiService) { }

    isAuth : boolean = false;

    async ngOnInit() {
        this.isAuth = await this.api.isAuthenticated();
    }

}