import { OnInit, Input } from "@angular/core";
import { Tender } from "src/app/models/tenders/tender";
import { DatetimeHelper } from "src/app/helpers/dateTime.helper";

export class BaseTenderGroupComponent implements OnInit{
    @Input() countryName: string;
    @Input() tendersAmount: number;
    @Input() tender: Tender
    @Input() renderHeader: boolean;

    constructor() { }

    ngOnInit() {
       
    }

    protected isOpened = false;
    dateToString(date: Date): string {

        return DatetimeHelper.toDateString(date);
    }

    getMonth(date: Date): string {
        return DatetimeHelper.toShortMonth(date).replace(' ', '.');
    }

    getHours(date: Date): string {
        return DatetimeHelper.toShortTime(date);
    }

    toggleMoreInfo(event) {
        event.preventDefault();

        this.isOpened = !this.isOpened;
    }

    get IsOpened() {
        return this.isOpened;
    }

    get ShowMoreSign(){
        return this.IsOpened ? '-' : '+'
    }

    get LoadingTime(){
        return `${DatetimeHelper.toShortTime(this.tender.loadingFrom)} - ${DatetimeHelper.toShortTime(this.tender.loadingTo)}`;
    }

    get TenderEndTime(){
        return DatetimeHelper.toShortTime(this.tender.endTime);
    }

}