import { OnInit, ChangeDetectorRef } from "@angular/core";
import { TenderGroup, Tender } from "src/app/models/tenders/tender";
import { TenderFilter } from "src/app/models/filters/tender.filter";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ApiService } from "src/app/services/api.service";
import { TenderDirections } from "src/app/models/direcitons/tender.directions";
import { CalendarFooterComponent } from "../../calendar-footer/calendar-footer.component";
import { FiltersHelper } from "src/app/helpers/tenders.filters.helper";

export abstract class BaseTendersComponent implements OnInit {

  calendarFooter = CalendarFooterComponent;
  protected tenders: Array<TenderGroup>;
  filters: TenderFilter;
  protected mainForm: FormGroup;
  isLoading = true;
  page: number = 1;
  private filteredTenders: Array<Tender> = new Array<Tender>();
  private _filtersHelper: FiltersHelper;
  query: string;

  selectCalendarRange: boolean = true;

  constructor(protected api: ApiService,
    protected formBuilder: FormBuilder,
    protected cd: ChangeDetectorRef) {

  }

  async ngOnInit() {
    const response = await this.loadData();

    this.tenders = response.tenders;
    this.filters = response.filters;

    this._filtersHelper = new FiltersHelper(this.tenders);

    this.initForm();

    this.isLoading = false;

  }

  abstract async loadData();

  protected onFilterChanges() {

  }

  initForm() {
    this.mainForm = this.formBuilder.group({
      countryTo: [],
      countryFrom: [],
      cityFrom: [],
      cityTo: [],
      export: [false],
      import: [false],
      internal: [false],
      unloadDate: [],
      once: [false],
      circle: [false],
      group: [false],
      contract: [false],
      dates: [null],
      currency: []
    });

    this.mainForm.valueChanges.subscribe(x => {
      if (this.selectCalendarRange && this.f.dates.value) {
        if (this.f.dates.value.start == null && this.f.dates.value.end == null) {
          this.f.dates.setValue(null);
        }
      }


      this.filterTenders();
      this.onFilterChanges();
    });

    this.filterTenders();


  }

  protected filterTenders() {

    this._filtersHelper.updateFilters(this.form.value);
    this.filteredTenders = this._filtersHelper.filteredTenders;
    this.filters = this._filtersHelper.filterOptions;

    this.cd.markForCheck();
  }

  get form() {
    return this.mainForm;
  }

  get f() {
    return this.form.controls;
  }

  get countriesFrom() {
    return this.filters.countriesFrom;
  }

  get citiesFrom() {
    return this.filters.citiesFrom;
  }

  get countriesTo() {
    return this.filters.countriesTo;
  }

  get citiesTo() {
    return this.filters.citiesTo;
  }


  getTendersByDirectionAmount(direction: TenderDirections) {

    let lots = new Array<Tender>();

    this.tenders.forEach(x => { lots = [...lots, ...x.items]; });

    switch (direction) {
      case TenderDirections.export:
        return lots.filter(x => this.isExport(x)).length;
      case TenderDirections.import:
        return lots.filter(x => this.isImport(x)).length;
      case TenderDirections.internal:
        return lots.filter(x => this.isInternal(x)).length;
    }
  }

  isExport(tender: Tender) {
    return tender.countryFromISO == "UA" && tender.countryToISO != "UA";
  }

  isImport(tender: Tender) {
    return tender.countryFromISO != "UA" && tender.countryToISO == "UA";
  }

  isInternal(tender: Tender) {
    return tender.countryFromISO == "UA" && tender.countryToISO == "UA";
  }


  get tendersList() {
    return this.filteredTenders;
  }

  getCountryTendersAmount(countryName: string): number {
    let country = this._filtersHelper.tenderGroups.filter(x => x.country === countryName);

    if (country.length > 0)
      return country[0].items.length;
  }

  isEmptyFilter(value): boolean {
    return value == null || value.length == 0 || value == false;
  }


  allowRenderHeader(tenderId: number) {
    return this._filtersHelper.tenderGroups.some(country => {
      return country.items[0].unicalNumber == tenderId;
    });
  }

  applySearch(query: string) {
    this.query = query;
  }

  private calendarDate;
  calendarOpened() {
    this.calendarDate = this.f.unloadDate.value;
  }

  calendatClosed() {
    if (this.calendarDate == this.f.unloadDate.value) {
      this.f.unloadDate.setValue(null)
      this.calendarDate = null;
    }
  }

  get filterState() {
    return this._filtersHelper.filterState;
  }

}