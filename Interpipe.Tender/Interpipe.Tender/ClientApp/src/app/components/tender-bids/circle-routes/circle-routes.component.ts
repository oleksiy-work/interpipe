import {
  Component,
  OnInit,
  Input,
  ViewChildren,
  QueryList,
  Output,
  EventEmitter,
  ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { TenderBid } from 'src/app/models/tenders/tender.bid';
import { FormGroup } from '@angular/forms';
import { CircleRoutesRowComponent } from './circle-routes-row/circle-routes-row.component';
import { ApiService } from 'src/app/services/api.service';
import { MakeBidReques } from 'src/app/models/request/bids/make.bid.reques';
import { CalendarFooterComponent } from '../../calendar-footer/calendar-footer.component';
import {TimeService} from "../../../services/time.service";

@Component({
  selector: 'app-circle-routes',
  templateUrl: './circle-routes.component.html',
  styleUrls: ['./circle-routes.component.css'],
})
export class CircleRoutesComponent implements OnInit {

  @Input() current: Array<TenderBid>;
  @Input() options: any;
  @Input() filterForm: FormGroup;
  @Input() filterState;
  @Output() onBidsSubmited = new EventEmitter<any>();
  @Output() onBidsSubmitFailed = new EventEmitter<any>();
  calendarFooter = CalendarFooterComponent;

  constructor(
    private api: ApiService,
  ) { }

  ngOnInit() {
    this.filterForm.valueChanges.subscribe(s => {
      this._pairs = null;
    });
  }

  private _pairs = null;
  get pairs() {
    if (!this._pairs) {
      this._pairs = this.current.filter(x => x.tenderBackUnicalNumber != null)
        .map(map => {

          return {
            first: map,
            second: this.current.find(f => {
              return f.unicalNumber == map.tenderBackUnicalNumber
            }
            )
          }
        });
    }

    return this._pairs;
  }

  @ViewChildren(CircleRoutesRowComponent) rows: QueryList<CircleRoutesRowComponent>;

  async submitBids() {
    let bids = new Array<MakeBidReques>();

    this.rows.toArray()
      .filter(x => x.isValidToSubmit())
      .forEach(x => { x.buildBidArray.forEach(f => { bids.push(f) }); x.bid.failedToSubmit = false; });

    try {
      let result = await this.api.makeBids(bids);
      this.onBidsSubmited.emit(result);
    }
    catch (error) {
      this.onBidsSubmitFailed.emit(error);
    }
  }

  get isAllValid() {
    if (this.rows)
      return this.rows.toArray()
        .filter(x => !x.bid.isFinished)
        .every(x => x.isValidToSubmit());

    return false;

  }

}
