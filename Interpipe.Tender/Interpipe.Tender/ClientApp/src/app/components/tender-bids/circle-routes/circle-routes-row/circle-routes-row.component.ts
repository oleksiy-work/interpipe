import { Component, OnInit, Input } from '@angular/core';
import { BidTableRowComponent } from '../../table-row/tender.bid.table.row.component';
import { ApiService } from 'src/app/services/api.service';
import { TenderBid } from 'src/app/models/tenders/tender.bid';
import { MakeBidReques } from 'src/app/models/request/bids/make.bid.reques';
import { MatSnackBar } from '@angular/material';
import { TimerService } from 'src/app/services/timer.service';
import { SignalrService } from '../../../../services/signalr.service';
import {SignalRService} from "../../../../services/signal.service";
import {TimeService} from "../../../../services/time.service";

@Component({
  selector: 'app-circle-routes-row',
  templateUrl: './circle-routes-row.component.html',
  styleUrls: ['./circle-routes-row.component.css']
})
export class CircleRoutesRowComponent extends BidTableRowComponent implements OnInit {


  @Input() backBid: TenderBid;
  newPrice2: 0;
  /*constructor(api: ApiService, snackBar: MatSnackBar, timerService: TimerService, public signalRService: SignalrService) {
    super(api, snackBar, timerService, signalRService);
  }*/
  constructor(api: ApiService, snackBar: MatSnackBar, timeService: TimeService, public signalRService: SignalRService) {
    super(api, snackBar, timeService, signalRService);
  }

 ngOnInit() {
    super.ngOnInit();
  }

  get IsSecondValid() {
    return this.isAllPricesValid;//this.newPrice2 > 0 && this.newPrice2 < (+this.backBid.price - +this.backBid.priceStep);
  }

  get IsPriceValid() {
    return this.isAllPricesValid;
  }

  get isAllPricesValid() {
    if (this.newPrice < this.bid.currenPrice && this.newPrice > 0 && this.newPrice2 > 0) {
      return (+this.newPrice + +this.newPrice2) <= (+this.bid.price + +this.backBid.price - +this.bid.priceStep);
    }

    return false;
  }

  async bidSubmitted(bid: number) {
    let tenders = new Array<MakeBidReques>();

    if (this.IsPriceValid) {
      tenders.push({
        comment: this.comment,
        currency: this.bid.currency,
        lotNumber: this.bid.lotNumber,
        unicalNumber: this.bid.unicalNumber,
        price: +this.newPrice
      });
    }

    if (this.IsSecondValid) {
      tenders.push({
        comment: '',
        currency: this.backBid.currency,
        lotNumber: this.backBid.lotNumber,
        unicalNumber: this.backBid.unicalNumber,
        price: +this.newPrice2
      });
    }

    if (tenders.length > 0) {
      try {
        var result = await this.api.makeBids(tenders);
      }
      catch (error) {
        this.bid.failedToSubmit = true;
        this.backBid.failedToSubmit = true;
        this.showError(error.error.map(x => `${x.unicalNumber} - ${x.errorMessage}`));
      }

      this.newPrice = 0;
      this.newPrice2 = 0;
      this.comment = '';
    }
  }

  isValidToSubmit(): boolean {
    return this.IsPriceValid && this.IsSecondValid;
  }

  get buildBidArray() {
    let tenders = new Array<MakeBidReques>();

    if (this.IsPriceValid) {
      tenders.push({
        comment: this.comment,
        currency: this.bid.currency,
        lotNumber: this.bid.lotNumber,
        unicalNumber: this.bid.unicalNumber,
        price: +this.newPrice
      });
    }

    if (this.IsSecondValid) {
      tenders.push({
        comment: '',
        currency: this.backBid.currency,
        lotNumber: this.backBid.lotNumber,
        unicalNumber: this.backBid.unicalNumber,
        price: +this.newPrice2
      });
    }

    return tenders;
  }


}
