/// <reference path="../../table-row/tender.bid.table.row.component.ts" />
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { BidTableRowComponent } from '../../table-row/tender.bid.table.row.component';
import { MakeBidReques } from 'src/app/models/request/bids/make.bid.reques';
import { MatSnackBar } from '@angular/material';
import { TimerService } from 'src/app/services/timer.service';
import { SignalrService } from '../../../../services/signalr.service';
import {SignalRService} from "../../../../services/signal.service";
import {TimeService} from "../../../../services/time.service";

@Component({
  selector: 'app-contract-routes-row',
  templateUrl: './contract-routes-row.component.html',
  styleUrls: ['./contract-routes-row.component.css']
})
export class ContractRoutesRowComponent extends BidTableRowComponent implements OnInit {
  vehicles: number = 0;
  /*constructor(api: ApiService, snackBar: MatSnackBar, timerService: TimerService, public signalRService: SignalrService) {
    super(api, snackBar, timerService, signalRService);
  }*/
  constructor(api: ApiService, snackBar: MatSnackBar, timeService: TimeService, public signalRService: SignalRService) {
    super(api, snackBar, timeService, signalRService);
  }
  ngOnInit() {
    super.ngOnInit();
  }

  async bidSubmitted(bid: number) {
    if (this.vehicles <= 0 || bid <= 0) {
      return
    }
    try {
      var result = await this.api.makeBids([{
        comment: this.comment,
        currency: this.bid.currency,
        lotNumber: this.bid.lotNumber,
        unicalNumber: this.bid.unicalNumber,
        price: +bid,
        vehiclesAmount: +this.vehicles
      }]);

    }
    catch (error) {
      this.showError(error.error.map(x => `${x.unicalNumber} - ${x.errorMessage}`));
      this.bid.failedToSubmit = true;
    }
    this.newPrice = 0;
    this.comment = '';
    this.vehicles = 0;
  }

  get IsPriceValid() {
    //console.log('helper', this.bid.myPrice);
    if (!this.priceHelper) {
      console.log('1');
      return false; }

    if (this.priceHelper.first === 0) {
      console.log('2');
      return false;
    }

    //if (this.bid.myPrice) {
    //  return this.newPrice <= this.bid.myPrice;
    //}
    if (this.priceHelper.first) {
      if (!this.bid.myPrice) {
        console.log('3');
        return true;
      } else {
        console.log('4');
        return this.newPrice <= this.bid.myPrice;
      }
    }
    if (this.priceHelper.first) {
      return true;
    }
    /*else {
      return this.newPrice < this.bid.currenPrice;
    }*/

  }

  isValidToSubmit(): boolean {
    return this.IsPriceValid && this.vehicles > 0;
  }

  get buildBid(): MakeBidReques {
    return {
      comment: this.comment,
      currency: this.bid.currency || '',
      lotNumber: this.bid.lotNumber,
      unicalNumber: this.bid.unicalNumber,
      price: +this.newPrice,
      vehiclesAmount: +this.vehicles
    };
  }
}
