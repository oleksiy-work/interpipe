import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'app-price-sender',
  templateUrl: './price-sender.component.html',
  styleUrls: ['./price-sender.component.css']
})
export class PriceSenderComponent implements OnInit, OnDestroy {

  @Input() price: number;
  @Input() myPrice: number;
  @Input() isReduction: boolean;
  @Input() enteredPrice: number = null;
  @Input() step: number;
  @Input() height: number;
  @Input() isMultiple: boolean;
  @Input() tenterType: string;
  @Input() startPrice: number;
  @Output() onBidSubmit = new EventEmitter<any>();

  private isSubmitTimeout: boolean = false;
  private submitTimeoutTime;
  constructor() { }


  ngOnInit() {
  }

  floorNumberDown() {
    let step = this.step;
    let price = this.enteredPrice > this.price - step ? this.price - step : this.enteredPrice;

    price = price > 0 ? price : 0;

    return price;
  }

  floorNumberUp() {
    let price = this.enteredPrice >= this.price ? this.price - this.step * 1 : this.enteredPrice;
    return Math.floor(price / this.step) * this.step + this.step;
  }

  get showFinalPrice() {
    if (this.tenterType === 'CON') {
      return true;
    }
    if (!this.isReduction)
      return true;

    return this.enteredPrice <= this.price - this.step;
  }

  get finalPrice() {
    return this.enteredPrice;
  }


  //URGENT!
  //Need to refactor.
  //Waitign for correct documentation about prices for each tender types.
  get first() {
    let price = this.price;
    if (this.tenterType === 'CON') {
      if (!this.myPrice && this.enteredPrice > 0) {
        return this.enteredPrice;
      } else {
        if (this.enteredPrice <= this.myPrice) {
          return this.enteredPrice;
        } else {
          return this.price - this.step;
        }
      }
    }
    if (this.isReduction) {
      if (this.tenterType == 'GRP') {
        if (this.myPrice > 0) {
          if (this.enteredPrice > this.myPrice || this.enteredPrice == 0) {
            return this.myPrice;
          }
        }

        if (!this.myPrice) {
          price = this.startPrice - this.step;
          if (this.enteredPrice == 0) {
            return price;
          }
          else {
            if (this.enteredPrice > this.startPrice - this.step) {
              return price;
            }
            else {
              return this.enteredPrice;
            }
          }
        }
        else {
          return this.enteredPrice == 0 ? this.startPrice - this.step : this.enteredPrice;
        }

        // else {
        //   price = this.enteredPrice > 0 ? this.enteredPrice : this.price;
        //   return price;
        // }
      }
      else {
        price = this.price;
      }

      let step = this.step;

      return this.enteredPrice == 0 ? price - step : this.floorNumberDown();
    }
    else {
      if (this.price <= 0)
        return this.enteredPrice;

      if (this.myPrice > 0) {
        if (this.enteredPrice > 0) {
          if (this.enteredPrice < this.myPrice) {
            return this.enteredPrice;
          }
        }
        return this.myPrice - this.step;
      }

      if (this.price > 0)
        return this.enteredPrice;
    }
  }

  get second() {
    return this.enteredPrice == 0 ? '0' : this.floorNumberUp();
  }

  get IsValid() {
    if (this.enteredPrice <= 0)
      return false;
    if (this.tenterType === 'CON') {
      if (!this.myPrice && this.enteredPrice > 0) {
        return true;
      } else {
        return this.enteredPrice <= this.myPrice;
      }
    }

    if (this.isReduction && this.tenterType !== 'CON') {
      return this.enteredPrice <= (this.myPrice ? this.myPrice - this.step : this.price - this.step);
    }
    else {
      if (this.price <= 0 && this.enteredPrice > 0) {
        return true;
      }

      //bid can not be more that our previous bid.
      if (this.myPrice > 0) {
        return this.enteredPrice > this.myPrice;
      }

      return true;
    }

    // if (this.isReduction) {

    //     if (this.enteredPrice <= this.price && this.enteredPrice > 0) {

    //         if (this.myPrice) {

    //             if (this.enteredPrice <= this.myPrice) {
    //                 if (this.enteredPrice == this.first || this.enteredPrice == this.second) {
    //                     return true;
    //                 }
    //             }

    //         }
    //         else {
    //             if (this.enteredPrice == this.first || this.enteredPrice == this.second) {
    //                 return true;
    //             }
    //         }

    //     }
    // }
    // else {
    //     if (this.myPrice) {
    //         if (this.enteredPrice > this.myPrice) {
    //             return true;
    //         }
    //     }
    //     else {
    //         return this.enteredPrice > this.price;
    //     }
    // }
  }

  get IsValidToSend() {
    if (this.enteredPrice <= 0 || this.first <= 0) {
      return false;
    }
    return this.IsValid;
  }

  get IsSenderValid() {
    return this.first > 0;
  }

  submitBid(event, bid) {

    event.preventDefault();

    if (this.isSubmitTimeout)
      return;

    if (bid <= 0)
      return;
    if (this.tenterType === 'CON') {
      if (this.myPrice && this.enteredPrice > this.myPrice) {
        return;
      }
    }
    this.onBidSubmit.emit(bid);

    this.submitTimeoutTime = setTimeout(() => { this.isSubmitTimeout = false }, 2000);
    this.isSubmitTimeout = true;
  }

  get ulStyle() {

    let style = {
      height: `${this.height}px`
    };

    return style;

  }

  ngOnDestroy(): void {
    if (this.submitTimeoutTime)
      clearTimeout(this.submitTimeoutTime);
  }

}
