import {Component, OnInit} from '@angular/core';
import {BidTableRowComponent} from '../../table-row/tender.bid.table.row.component';
import {ApiService} from 'src/app/services/api.service';
import {MatSnackBar} from '@angular/material';
import {SignalRService} from "../../../../services/signal.service";
import {TimeService} from "../../../../services/time.service";

@Component({
  selector: 'app-simple-routes-row',
  templateUrl: './simple-routes-row.component.html',
  styleUrls: ['./simple-routes-row.component.css'],
})
export class SimpleRoutesRowComponent extends BidTableRowComponent implements OnInit {

  /*constructor(api: ApiService, snackBar: MatSnackBar, timerService: TimerService, public signalRService: SignalrService) {
    super(api, snackBar, timerService, signalRService);
  }*/
  constructor(api: ApiService, snackBar: MatSnackBar, timeService: TimeService, public signalRService: SignalRService) {
    super(api, snackBar, timeService, signalRService);
  }
  ngOnInit() {
    super.ngOnInit();
  }

  isValidToSubmit(): boolean {
    return this.IsPriceValid;
  }

}
