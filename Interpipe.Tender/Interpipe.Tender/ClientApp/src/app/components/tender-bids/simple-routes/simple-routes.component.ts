import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  EventEmitter,
  Input, OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {SimpleRoutesRowComponent} from './simple-routes-row/simple-routes-row.component';
import {ApiService} from 'src/app/services/api.service';
import {CalendarFooterComponent} from '../../calendar-footer/calendar-footer.component';
import {TimeService} from "../../../services/time.service";
import {pipe, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: 'app-simple-routes',
  templateUrl: './simple-routes.component.html',
  styleUrls: ['./simple-routes.component.css'],
})
export class SimpleRoutesComponent implements OnInit {

  @Input() current: Array<any>
  @Input() options: any;
  @Input() filterForm: FormGroup;
  @Input() filterState;

  @Output() onBidsSubmited = new EventEmitter<any>();
  @Output() onBidsSubmitFailed = new EventEmitter<any>();
  calendarFooter = CalendarFooterComponent;
  constructor(
    private api: ApiService,

  ) { }

  ngOnInit() {
  }
  @ViewChildren(SimpleRoutesRowComponent) rows: QueryList<SimpleRoutesRowComponent>;

  async submitBids() {
    let allBids = this.rows.toArray();

    allBids.forEach(x => x.bid.failedToSubmit = false);

    var bids = allBids.filter(x => x.isValidToSubmit()).map(x => x.buildBid);

    try {

      var result = await this.api.makeBids(bids);

      this.onBidsSubmited.emit(result);

    }
    catch (error) {
      this.onBidsSubmitFailed.emit(error);
    }
  }

  get isAllValid() {
    if (this.rows)
      return this.rows.toArray()
        .filter(x => !x.bid.isFinished)
        .every(x => x.isValidToSubmit());

    return false;

  }

}
