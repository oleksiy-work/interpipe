import { Component, OnInit, ViewChildren, QueryList, ViewChild, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { BidsInfoRequest } from 'src/app/models/request/bids/bids.reques.info';
import { TenderBid } from 'src/app/models/tenders/tender.bid';
import { BidTableRowComponent } from './table-row/tender.bid.table.row.component';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TenderFilter } from 'src/app/models/filters/tender.filter';
import { DateAdapter } from 'saturn-datepicker';
import { ContractRoutesRowComponent } from './contract-routes/contract-routes-row/contract-routes-row.component';
import { GroupRoutesRowComponent } from './group-rotues/group-routes-row/group-routes-row.component';
import { CircleRoutesRowComponent } from './circle-routes/circle-routes-row/circle-routes-row.component';
import { SimpleRoutesRowComponent } from './simple-routes/simple-routes-row/simple-routes-row.component';
import { ContractRoutesComponent } from './contract-routes/contract-routes.component';
import { GroupRotuesComponent } from './group-rotues/group-rotues.component';
import { CircleRoutesComponent } from './circle-routes/circle-routes.component';
import { SimpleRoutesComponent } from './simple-routes/simple-routes.component';
import { BidsFilterHelper } from 'src/app/helpers/bids.filter.helper';
import { MatSnackBar } from '@angular/material';
import { ErrorSnackbarComponent } from '../error-snackbar/error-snackbar.component';
import {Observable, Subject, Subscription} from 'rxjs';
import { TimerService } from '../../services/timer.service';
import {takeUntil} from "rxjs/operators";
import {TimeService} from "../../services/time.service";
import * as signalR from '@aspnet/signalr';

@Component({
    selector: 'app-tender-bids',
    templateUrl: 'tender.bid.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TenderBidsComponent implements OnInit, OnDestroy {
    unsubscribe$ = new Subject<void>();
    private connectionIsEstablished = false;
    private _hubConnection: HubConnection;
    private tabIndex: number = 1;
    private mainForm: FormGroup;
    private filteredTenders: Array<TenderBid> = new Array<TenderBid>();
    private tendersSortTimer;

    private   serverDate: Date = new Date();
    private   _serverDateSubscription: Subscription;
    private   curSecond: number = 0;
    private   updateTimer: Observable<number>;
    private   udpateTimerSubscription: Subscription;

    serverTimeString: string = '00.00.00';
    localTimeString: string = '00.00.00';
    localTimeStringUTC;
    isLoaded: boolean = false;

    private _filtersHelper: BidsFilterHelper;

    constructor(private api: ApiService, private router: Router,
        private route: ActivatedRoute, private formBuilder: FormBuilder,
        private dateAdapter: DateAdapter<Date>,
        private cd: ChangeDetectorRef,
        private snackBar: MatSnackBar,
        private timer: TimerService,
        private timeService: TimeService
    ) {


        /*this._hubConnection.onclose(() => {
            console.log("Tender hub onclose, reconnecting");
            setTimeout(() => { this.startConnection(); }, 3000);
        });*/


        this.dateAdapter.getFirstDayOfWeek = () => { return 1; }

        this.dateAdapter.setLocale("ru-RU");
    }

    private current: Array<TenderBid>;

    @ViewChildren(ContractRoutesComponent) contractRows: QueryList<ContractRoutesComponent>;
    @ViewChildren(GroupRotuesComponent) grouptRows: QueryList<GroupRotuesComponent>;
    @ViewChildren(CircleRoutesComponent) circleRows: QueryList<CircleRoutesComponent>;
    @ViewChildren(SimpleRoutesComponent) simpleRows: QueryList<SimpleRoutesComponent>;

    async ngOnInit() {
      this.timeService._timer
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          () => {
            this.cd.detach();
            setTimeout(() => {
              this.cd.detectChanges();
              this.cd.reattach();
            }, 20);
          }
        );
      //this._serverDateSubscription = this.api._serverDateChanged$.subscribe(d => {
      //  this.serverDate = d;
      //  this.curSecond = 0;

      //  this.updateTime();
      //});


      //this.udpateTimerSubscription = this.timer.timer.subscribe(x => {
      //  this.updateTime();
      //});
      this.udpateTimerSubscription = this.timer.timer.subscribe(x => {
      this.updateTime();
      });

        if (!await this.api.isAuthenticated()) {
          this.router.navigate(['/']);
        }


        let data = new Array<BidsInfoRequest>();

        Object.keys(window.history.state).forEach(key => {
            if (key !== 'navigationId') {
                data.push({ lotNumber: window.history.state[key].lotNumber, unicalNumber: window.history.state[key].unicalNumber, tenderBackUnicalNumber: window.history.state[key].tenderBackUnicalNumber });
            }
        });
        try {
            if (data.length == 0) {
                let localData = this.getData();

                if (localData) {
                    data = localData;
                }
                else {
                    this.router.navigate(['/']);
                }
            }

            let response = await this.api.buildBidsInfo(data);
            this.current = response.bids;
          this.saveData(data);
            this._filtersHelper = new BidsFilterHelper(response.bids);
          this.startConnection();
          this.registerOnServerEvents();
          this.isLoaded = true;
        }
        catch (error) {
            this.router.navigate(['/']);
        }
      this.initForm();




        // this.tendersSortTimer = setInterval(() => {
        //     this.sortTendets();
        // }, 1000);

        this.sortTendets();

        this.resolveParametersTab();
      this.cd.detectChanges();
  }

  updateTime() {
      this.localTimeStringUTC = new Date().getTime();
    let locTime = new Date(Date.now());

    let seconds = locTime.getSeconds();
    let minutes = locTime.getMinutes();
    let hours = locTime.getHours();

    this.localTimeString = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  }

  //updateTime() {
  //  if (this.serverDate) {
  //    if (this.curSecond >= 60) {
  //      this.curSecond = 0;
  //    }
  //    this._updateTime();
  //  }
  //}

  //private _updateTime() {
  //  this.curSecond++;

  //  let serverTime = new Date(this.serverDate.getTime() + this.curSecond * 1000);


  //  let seconds = serverTime.getSeconds();
  //  let minutes = serverTime.getMinutes();
  //  let hours = serverTime.getHours();

  //  this.serverTimeString = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

  //  let locTime = new Date(Date.now());

  //  seconds = serverTime.getSeconds();
  //  minutes = serverTime.getMinutes();
  //  hours = serverTime.getHours();

  //  this.localTimeString = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;

  //}

    private resolveParametersTab() {
        this.route.params.subscribe(data => {
            if (data.tab) {
                switch (data.tab) {
                    case 'one-time':
                        this.switchTab(null, 1);
                        break;
                    case 'loop':
                        this.switchTab(null, 2);
                        break;
                    case 'grouped':
                        this.switchTab(null, 3);
                        break;
                    case 'contract':
                        this.switchTab(null, 4);
                        break;
                }
            }
          this.cd.markForCheck();

        })
    }

    ngOnDestroy(): void {
        if (this.tendersSortTimer) {
          clearInterval(this.tendersSortTimer);
        }
      this.udpateTimerSubscription.unsubscribe();
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
      this._hubConnection.stop().then( () => {});
    }

    initForm() {
        this.mainForm = this.formBuilder.group({
            countryTo: [],
            countryFrom: [],
            cityFrom: [],
            cityTo: [],
            export: [false],
            import: [false],
            internal: [false],
            dates: [],
            once: [false],
            circle: [false],
            group: [false],
            contract: [false],
            currency: [],
        });



        this.mainForm.valueChanges.subscribe(x => {
            if (this.f.dates.value) {
                if (this.f.dates.value.start == null && this.f.dates.value.end == null) {
                    this.f.dates.setValue(null);
                }
            }

            this.filterTenders();
          this.cd.markForCheck();
        })

        this.filterTenders();
      this.cd.markForCheck();
    }


    protected filterTenders() {

        this._filtersHelper.updateFilters(this.form.value);
        this.filteredTenders = this._filtersHelper.filteredTenderBids;
        this.sortTendets();
      this.cd.markForCheck();
    }

    protected sortTendets() {
        this.filteredTenders = this.filteredTenders.sort((a, b) => {

            if (a.isFinished < b.isFinished)
                return -1;

            return 1;
        });
    }

    get tenders() {
        return this._filtersHelper.filteredTenderBids;
    }


    get tabType() {
        switch (this.tab) {
            case 1:
                return '';
            case 2:
                return 'RNG';
            case 3:
                return 'GRP'
            case 4:
                return 'CON'
        }
    }

    getTabTendersAmount(tabIndex: number) {
        return this._filtersHelper.getTabTendersAmount(tabIndex, tabIndex == this.tabIndex);
    }



    get form() {
        return this.mainForm;
    }

    get f() {
        return this.form.controls;
    }


    get tab() {
        return this.tabIndex;
    }


    get IsAllValid() {
        try {
            return this.isRowValid;
        }
        catch (err) {
            // console.error(err);
        }

        return false;
    }

    get isRowValid() {
        let row;
        switch (this.tabIndex) {
            case 1:
                row = this.simpleRows.toArray()[0];
                break;
            case 2:
                row = this.circleRows.toArray()[0];
                break;
            case 3:
                row = this.grouptRows.toArray()[0];
                break;
            case 4:
                row = this.contractRows.toArray()[0];
                break;
        }

        return row && row.isAllValid;
    }

    async sendAllBids(event) {
        event.preventDefault();
        try {
            if (this.IsAllValid) {
                switch (this.tabIndex) {
                    case 1:
                        return this.simpleRows.toArray()[0].submitBids();
                    case 2:
                        return this.circleRows.toArray()[0].submitBids();
                    case 3:
                        return this.grouptRows.toArray()[0].submitBids();
                    case 4:
                        return this.contractRows.toArray()[0].submitBids();
                }
            }
          this.cd.markForCheck();

        }
        catch (error) {

        }
    }
    private dataKey = 'selected-tenders'
    saveData(data) {
        sessionStorage.setItem(this.dataKey, JSON.stringify(data));
    }

    getData(): BidsInfoRequest[] {
        return JSON.parse(sessionStorage.getItem(this.dataKey));
    }

    async updateData() {
        let data = this.getData();
        var loaded = await this.api.buildBidsInfo(data);
        loaded.bids.forEach(x => {
            this.current.forEach(current => {

                if (current.number == x.number && current.lotNumber == x.lotNumber) {
                    current.myPrice = x.myPrice;
                    current.price = x.price;
                    current.myCarsAmount = x.myCarsAmount;
                    current.status = x.status;
                }

            });
        });
      this.cd.markForCheck();

    }

    switchTab(event, tab) {
        if (event) {
            event.preventDefault();
        }

        if (tab == this.tabIndex)
            return;

        this.tabIndex = tab;
        this.initForm();
        switch (tab) {
            case 1:
                this.f.once.setValue(true);
                break;
            case 2:
                this.f.circle.setValue(true);
                break;
            case 3:
                this.f.group.setValue(true);
                break;
            case 4:
                this.f.contract.setValue(true);
                break;
        }
        this._filtersHelper.changeTab(tab);
      this.cd.markForCheck();
    }

    get simple() {
        return this._filtersHelper.simple;
    }

    get circle() {
        return this._filtersHelper.circle;
    }

    get group() {
        return this._filtersHelper.group;
    }

    get contract() {
        return this._filtersHelper.contract;
    }

    get countriesFrom() {
        return this._filtersHelper.filterOptions.countriesFrom;

    }

    get citiesFrom() {
        return this._filtersHelper.filterOptions.citiesFrom;
    }

    get countriesTo() {
        return this._filtersHelper.filterOptions.countriesTo;
    }

    get citiesTo() {
        return this._filtersHelper.filterOptions.citiesTo;
    }

    get options() {
        return {
            countriesFrom: this.countriesFrom,
            citiesFrom: this.citiesFrom,
            countriesTo: this.countriesTo,
            citiesTo: this.citiesTo
        }
    }

    get filtersState() {
        return this._filtersHelper.filterState;
    }

    private registerOnServerEvents(): void {
        this._hubConnection.on('PriceUpdated', (data: any) => {
            this.updateData();
        });
    }
  private createConnection() {
     return new HubConnectionBuilder()
       .withUrl(window.location.origin + '/prices')

      .build();
  }
  private startConnection(): void {
    let cdx = this;
    this._hubConnection = this.createConnection();
    this._hubConnection
        .start()
        .then(() => {
            this.connectionIsEstablished = true;
            console.log('Tender Hub connection started');
          // this._hubConnection.serverTimeoutInMilliseconds = 1000 * 60 * 10;
          let numbers = this.current.map(x => `${x.unicalNumber}`);
          this.subscribeToUpdates(numbers);
        })
        .catch(err => {
            console.log('Tender Hub Error while establishing connection, retrying...', err);
            setTimeout(function () { cdx.startConnection(); }, 3000);
        });
    }

    private subscribeToUpdates(numbers) {
      return this._hubConnection.invoke('SubscribePriceBid', numbers)
          .then(() => { console.log('message sent successfully'); })
          .catch((err) => console.log('error while sending a message: ' + err));
    }

    onBidsSubmitted(result) {
        this.cd.markForCheck();
    }

    async onBidSubmitFailed(exception) {
        let error = exception.error;
        this.showError(error.map(x => `${x.unicalNumber} - ${x.errorMessage}`));
        if (error) {
            error.forEach(err => {
                let tender = this.filteredTenders.find(x => x.unicalNumber == err.unicalNumber);
                if (tender) {
                    tender.failedToSubmit = true;
                }
            });
        }
        await this.updateData();
      this.cd.detectChanges();
    }

    showError(errors: string[]) {
        this.snackBar.openFromComponent(ErrorSnackbarComponent, { data: errors });
      this.cd.detectChanges();
  }

}
