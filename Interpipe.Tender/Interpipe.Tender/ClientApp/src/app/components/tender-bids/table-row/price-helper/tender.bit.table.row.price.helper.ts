import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: '[app-tender-bid-row-tender-helper]',
    templateUrl: 'tender.bit.table.row.price.helper.html'
})
export class TenderBidHelperComponent implements OnInit {

    @Input() price: number;
    @Input() myPrice: number;
    @Input() isReduction: boolean;
    @Input() enteredPrice: number = null;
    @Input() step: number;
    @Input() height: number;
    @Output() onBidSubmit = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {

    }

    floorNumberDown() {
        let price = this.enteredPrice >= this.price ? this.price - this.step * 1 : this.enteredPrice;

        return Math.floor(price / this.step) * this.step;
    }

    floorNumberUp() {
        let price = this.enteredPrice >= this.price ? this.price - this.step * 1 : this.enteredPrice;
        return Math.floor(price / this.step) * this.step + this.step;
    }

    get showFinalPrice() {
        if (!this.isReduction)
            return true;

        return this.enteredPrice <= this.price - this.step;
    }

    get finalPrice() {
        return this.enteredPrice;
    }


    get first() {
        return this.enteredPrice == 0 ? '&nbsp;' : this.floorNumberDown();
    }

    get second() {
        return this.enteredPrice == 0 ? '&nbsp;' : this.floorNumberUp();
    }

    get IsValid() {
        if (this.enteredPrice == 0) {

            return true;
        }

        if (this.isReduction) {
            return this.enteredPrice <= (this.price - this.step);
        }
        else {
            return this.price ? this.enteredPrice <= this.price : true;
        }

        // if (this.isReduction) {

        //     if (this.enteredPrice <= this.price && this.enteredPrice > 0) {

        //         if (this.myPrice) {

        //             if (this.enteredPrice <= this.myPrice) {
        //                 if (this.enteredPrice == this.first || this.enteredPrice == this.second) {
        //                     return true;
        //                 }
        //             }

        //         }
        //         else {
        //             if (this.enteredPrice == this.first || this.enteredPrice == this.second) {
        //                 return true;
        //             }
        //         }

        //     }
        // }
        // else {
        //     if (this.myPrice) {
        //         if (this.enteredPrice > this.myPrice) {
        //             return true;
        //         }
        //     }
        //     else {
        //         return this.enteredPrice > this.price;
        //     }
        // }
        return false;

    }

    get IsValidToSend() {
        if (this.enteredPrice == 0)
            return false;
        return this.IsValid
    }

    submitBid(event, bid) {

        event.preventDefault();
        if (bid > this.price)
            return;
        this.onBidSubmit.emit(bid);
    }

    get ulStyle() {

        let style = {
            height: `${this.height}px`
        };

        return style;

    }

}