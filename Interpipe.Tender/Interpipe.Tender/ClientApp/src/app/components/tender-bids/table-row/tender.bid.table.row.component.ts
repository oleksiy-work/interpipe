import { Component, OnInit, Input, ViewChild, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { TenderBid } from 'src/app/models/tenders/tender.bid';
import { DatetimeHelper } from 'src/app/helpers/dateTime.helper';
import { TenderBidHelperComponent } from './price-helper/tender.bit.table.row.price.helper';
import { ApiService } from 'src/app/services/api.service';
import { MakeBidReques } from 'src/app/models/request/bids/make.bid.reques';
import { PriceSenderComponent } from '../price-sender/price-sender.component';
import { MatSnackBar } from '@angular/material';
import { ErrorSnackbarComponent } from '../../error-snackbar/error-snackbar.component';
import {Observable, of, Subject, Subscription, timer} from 'rxjs';
import { TimerService } from 'src/app/services/timer.service';
import { SignalrService } from '../../../services/signalr.service';
import {exhaustMap, switchMap, takeUntil} from "rxjs/operators";
import {SignalRService} from "../../../services/signal.service";
import {TimeService} from "../../../services/time.service";

// @Component({
//     selector: 'app-bid-table-row',
//     templateUrl: './tender.bid.table.row.component.html'
// })
export abstract class BidTableRowComponent implements OnInit, OnDestroy {

  @Input() bid: TenderBid;

  protected isOpened: boolean = false;
  newPrice: number = 0;
  comment: string;
  disableGreen = true;

  @ViewChild(PriceSenderComponent, { static: false }) priceHelper: PriceSenderComponent;

  private serverDate: Date = new Date(Date.now());
  private _serverDateSubscription: Subscription;
  unsubscribe$ = new Subject<void>();

  constructor(
    protected api: ApiService,
    private _snackBar: MatSnackBar,
    protected timeService: TimeService,
    // public signalRService: SignalrService,
    private signalrService: SignalRService
  ) {}


  protected timeToClose: any = null;
  protected curSecond = 0;

  protected updateTimer: Observable<number>;
  protected udpateTimerSubscription: Subscription;

  protected closeDate: Date;

  protected busy: boolean = false;

  private _initSub: boolean = false;

  ngOnInit() {
    console.log(this.bid.endTimeUtc);
    this.closeDate = new Date(this.bid.endTimeUtc);
    console.log(this.closeDate);
    this.signalrService.getMessage()
      .pipe(
        takeUntil(this.unsubscribe$),
        exhaustMap( (res: any) => {
          console.log('signalR tenderTimeHub');
          console.log(res);
          this.serverDate = new Date(res);
          console.log(this.serverDate);
          // this.updateTime(this.serverDate);
          this._initTimerSubscription(this.serverDate);
          return of();
        })
      )
      .subscribe( res => {
      });

    /*this.signalRService.onSignalRMessage
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe((data: any) => {
      this.serverDate = data;
      //console.log(this.serverDate);
      this.updateTime(this.serverDate);
    });*/


    //this._serverDateSubscription = this.api._serverDateChanged$.subscribe(d => {
    //  this.serverDate = d;
    //  this.curSecond = 0;

    //  this.updateTime();
    //});


    //this.udpateTimerSubscription = this.timerService.timer.subscribe(x => {
    //  this.updateTime();
    //});
  }

  totalTimeToClose = 0;
  timeDiff = 0;

  updateTime(serverDate) {
    let diff = this.closeDate.getTime() - serverDate.getTime();
    this.totalTimeToClose = diff;
    if (diff <= 0) {
      this.timeToClose = null;
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
      return;
    }
    this.startCountDown(diff);
    /*if (diff < 5000) {
      this.busy = true;
      this.disableGreen = false;
    }

    let seconds = Math.floor((diff / 1000) % 60);
    let minutes = Math.floor((diff / (1000 * 60)) % 60);
    let hours = Math.floor((diff / (1000 * 60 * 60) % 24));

    this.timeToClose = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    *///this.cdr.markForCheck();
  }
  startCountDown(diff) {
    /*const interval = setInterval(() => {
      console.log(diff);
      diff = diff - 1000;
      if (diff < 0 ) {
        clearInterval(interval);
        console.log('Ding!');
        this.timeToClose = null;
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        return;
      }
      this.timeToClose = diff;
      console.log(this.timeToClose);
    }, 1000);*/
    this.timeToClose = diff;
    this.timeService._timer
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe( () => {
      this.timeToClose = this.timeToClose - 1000;
      if (this.timeToClose <= 0) {
        this.timeToClose = null;
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        return;
      }
    });
  }

  private _initTimerSubscription(serverDate): void {
    console.log(serverDate);
    let diff = this.closeDate.getTime() - serverDate.getTime();
    this.totalTimeToClose = diff;
    if (diff <= 0) {
      this._unsub();
      return;
    }
    this.timeToClose = new Date(diff);
    console.log(this.timeToClose);
    if (!this._initSub) {
      this.timeService._timer
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe( () => {
          this.timeToClose = this.timeToClose - 1000;
          if (this.timeToClose <= 0) {
            this._unsub();
            return;
          }
        });
      this._initSub = true;
    }
  }

  private _unsub(): void {
    this.timeToClose = null;
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  //updateTime() {
  //  if (!this.closeDate) {
  //    this.closeDate = new Date(this.bid.endTimeUtc);
  //  }

  //  if (this.serverDate) {
  //    if (this.curSecond >= 60) {
  //      this.curSecond = 0;
  //    }
  //    this._updateTime();
  //  }
  //}

  //private _updateTime() {
  //    this.curSecond++;

  //    let now = new Date(this.serverDate.getTime() + this.curSecond * 1000);

  //    let diff = this.closeDate.valueOf() - now.valueOf();

  //    this.timeDiff = 0;

  //    this.totalTimeToClose = diff;

  //    if (diff <= 0) {
  //      this.timeToClose = '00:00:00';
  //      return;
  //    }

  //    if (diff < 5000) {
  //      this.busy = true;
  //    }

  //    let seconds = Math.floor((diff / 1000) % 60);
  //    let minutes = Math.floor((diff / (1000 * 60)) % 60);
  //    let hours = Math.floor((diff / (1000 * 60 * 60)));

  //    this.timeToClose = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  //}

  get isEnabled() {
    return this.timeToClose > 0;
  }

  get IsOpened() {
    return this.isOpened;
  }

  toggleOpened(event) {
    event.preventDefault();
    this.isOpened = !this.isOpened;
  }

  dateToString(date: Date): string {

    return DatetimeHelper.toDateString(date);
  }

  getMonth(date: Date): string {
    return DatetimeHelper.toShortMonth(date).replace(' ', '.');
  }

  getHours(date: Date): string {
    return DatetimeHelper.toShortTime(date);
  }
  get LoadingTime() {
    return `${DatetimeHelper.toShortTime(this.bid.loadingFrom)} - ${DatetimeHelper.toShortTime(this.bid.loadingTo)}`;
  }

  get TenderEndTime() {
    return DatetimeHelper.toShortTime(this.bid.tenderEndTime);
  }

  get TimeToEnd() {
    return this.timeToClose;

  }

  get IsPriceValid() {
    if (!this.priceHelper) { return true; }
    return this.priceHelper.IsValidToSend;
  }

  async bidSubmitted(bid: number) {
    try {
      this.bid.failedToSubmit = false;
      var result = await this.api.makeBids([{
        comment: this.comment,
        currency: this.bid.currency,
        lotNumber: this.bid.lotNumber,
        unicalNumber: this.bid.unicalNumber,
        price: +bid
      }]);
    }
    catch (error) {
      this.bid.failedToSubmit = true;
      this.showError(error.error.map(x => `${x.unicalNumber} - ${x.errorMessage}`));
    }

    this.newPrice = 0;
    this.comment = '';
  }

  get buildBid(): MakeBidReques {
    return {
      comment: this.comment,
      currency: this.bid.currency || '',
      lotNumber: this.bid.lotNumber,
      unicalNumber: this.bid.unicalNumber,
      price: +this.newPrice
    };
  }

  get showTwoRowsInPrice() {
    if (this.priceHelper)
      return !this.priceHelper.showFinalPrice;

    return false;
  }

  get areMyPriceValid() {
    return this.bid.myPrice && this.bid.myPrice <= this.bid.price;
  }

  get IsSenderValid() {
    if (this.priceHelper)
      return this.priceHelper.IsSenderValid;

    return false;
  }

  protected showError(errors: string[]) {
    this._snackBar.openFromComponent(ErrorSnackbarComponent, {
      data: errors
    });
  }

  abstract isValidToSubmit(): boolean;

  ngOnDestroy(): void {
    if (this.udpateTimerSubscription) {
      this.udpateTimerSubscription.unsubscribe();
      this.udpateTimerSubscription = null;
    }
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
