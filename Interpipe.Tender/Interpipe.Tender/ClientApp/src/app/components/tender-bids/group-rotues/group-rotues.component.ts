import {
  Component,
  OnInit,
  Input,
  ViewChildren,
  QueryList,
  Output,
  EventEmitter,
  ChangeDetectionStrategy, ChangeDetectorRef
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GroupRoutesRowComponent } from './group-routes-row/group-routes-row.component';
import { ApiService } from 'src/app/services/api.service';
import { CalendarFooterComponent } from '../../calendar-footer/calendar-footer.component';
import {TimeService} from "../../../services/time.service";

@Component({
  selector: 'app-group-rotues',
  templateUrl: './group-rotues.component.html',
  styleUrls: ['./group-rotues.component.css'],
})
export class GroupRotuesComponent implements OnInit {

  @Input() current: Array<any>;
  @Input() options: any;
  @Input() filterForm: FormGroup;
  @Input() filterState;
  @Output() onBidsSubmited = new EventEmitter<any>();
  @Output() onBidsSubmitFailed = new EventEmitter<any>();
  calendarFooter = CalendarFooterComponent;

  constructor(
    private api: ApiService,
  ) { }

  ngOnInit() {
  }

  @ViewChildren(GroupRoutesRowComponent) rows: QueryList<GroupRoutesRowComponent>;

  async submitBids() {
    let allBids = this.rows.toArray();
    allBids.forEach(x => x.bid.failedToSubmit = false);

    var bids = allBids.filter(x => x.isValidToSubmit()).map(x => x.buildBid);
    try {
      var result = await this.api.makeBids(bids);
      this.onBidsSubmited.emit(result);
    }
    catch (error) {
      this.onBidsSubmitFailed.emit(error);
    }
  }

  get isAllValid() {
    if (this.rows)
      return this.rows.toArray()
        .filter(x => !x.bid.isFinished)
        .every(x => x.isValidToSubmit());

    return false;

  }

}
