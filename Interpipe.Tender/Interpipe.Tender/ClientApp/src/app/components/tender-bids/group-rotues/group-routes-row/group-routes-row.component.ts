import { Component, OnInit } from '@angular/core';
import { BidTableRowComponent } from '../../table-row/tender.bid.table.row.component';
import { ApiService } from 'src/app/services/api.service';
import { MakeBidReques } from 'src/app/models/request/bids/make.bid.reques';
import { MatSnackBar } from '@angular/material';
import { TimerService } from 'src/app/services/timer.service';
import { SignalrService } from '../../../../services/signalr.service';
import {SignalRService} from "../../../../services/signal.service";
import {TimeService} from "../../../../services/time.service";

@Component({
  selector: 'app-group-routes-row',
  templateUrl: './group-routes-row.component.html',
  styleUrls: ['./group-routes-row.component.css']
})
export class GroupRoutesRowComponent extends BidTableRowComponent implements OnInit {

  vehicles: number = 0;
  /*constructor(api: ApiService, snackBar: MatSnackBar, timerService: TimerService, public signalRService: SignalrService) {
    super(api, snackBar, timerService, signalRService);
  }*/
  constructor(api: ApiService, snackBar: MatSnackBar, timeService: TimeService, public signalRService: SignalRService) {
    super(api, snackBar, timeService, signalRService);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  async bidSubmitted(bid: number) {
    if (this.isValidToSubmit()) {
      try {
        var result = await this.api.makeBids([{
          comment: this.comment,
          currency: this.bid.currency,
          lotNumber: this.bid.lotNumber,
          unicalNumber: this.bid.unicalNumber,
          price: +bid,
          vehiclesAmount: +this.vehicles
        }]);
      }
      catch (error) {
        this.showError(error.error.map(x => `${x.unicalNumber} - ${x.errorMessage}`));
        this.bid.failedToSubmit = true;
      }

      this.newPrice = 0;
      this.comment = '';
      this.vehicles = 0;

    }
  }

  isValidToSubmit(): boolean {
    return this.IsSenderValid && this.vehicles > 0;
  }

  get buildBid(): MakeBidReques {
    return {
      comment: this.comment,
      currency: this.bid.currency || '',
      lotNumber: this.bid.lotNumber,
      unicalNumber: this.bid.unicalNumber,
      price: +this.newPrice,
      vehiclesAmount: +this.vehicles
    };
  }

}
