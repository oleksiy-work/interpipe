import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { UnauthorizedTendersComponent } from './components/tenders/unauthorized/unauthorized.tenders.component';
import { APP_BASE_HREF } from '@angular/common';
import { ApiService } from './services/api.service';

import { NgSelectModule } from '@ng-select/ng-select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatGridListModule } from '@angular/material/grid-list';
import { UnauthorizedTenderGroupComponent } from './components/tenders/unauthorized/tender-group/unauthorized.tender.group.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { AuthorizedTendersComponent } from './components/tenders/authorized/authorized.tenders.component';
import { AuthorizedTenderGroupComponent } from './components/tenders/authorized/tender-group/authorized.tender.group.component';
import { TenderBidsComponent } from './components/tender-bids/tender.bid.component';
import { BidTableRowComponent } from './components/tender-bids/table-row/tender.bid.table.row.component';
import { TenderBidHelperComponent } from './components/tender-bids/table-row/price-helper/tender.bit.table.row.price.helper';
import { OnlyNumber } from './directives/number.only';
import { TenderComponent } from './components/tenders/tender.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MAT_DATE_LOCALE } from '@angular/material';
import { SimpleRoutesComponent } from './components/tender-bids/simple-routes/simple-routes.component';
import { SimpleRoutesRowComponent } from './components/tender-bids/simple-routes/simple-routes-row/simple-routes-row.component';
import { PriceSenderComponent } from './components/tender-bids/price-sender/price-sender.component';
import { CircleRoutesComponent } from './components/tender-bids/circle-routes/circle-routes.component';
import { CircleRoutesRowComponent } from './components/tender-bids/circle-routes/circle-routes-row/circle-routes-row.component';
import { GroupRotuesComponent } from './components/tender-bids/group-rotues/group-rotues.component';
import { GroupRoutesRowComponent } from './components/tender-bids/group-rotues/group-routes-row/group-routes-row.component';
import { ContractRoutesComponent } from './components/tender-bids/contract-routes/contract-routes.component';
import { ContractRoutesRowComponent } from './components/tender-bids/contract-routes/contract-routes-row/contract-routes-row.component';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { CalendarFooterComponent } from './components/calendar-footer/calendar-footer.component';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { ErrorSnackbarComponent } from './components/error-snackbar/error-snackbar.component';
import { MatIconModule } from '@angular/material/icon';
import { TimerService } from './services/timer.service';
import { SignalrService } from './services/signalr.service';
import {SignalRService} from "./services/signal.service";
import { TimeService } from "./services/time.service";
import { TranslateModule, TranslateLoader, TranslateStore, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader/';
import { LocalizationService } from './services/localization.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    UnauthorizedTendersComponent,
    UnauthorizedTenderGroupComponent,
    AuthorizedTendersComponent,
    AuthorizedTenderGroupComponent,
    TenderBidsComponent,
    TenderBidHelperComponent,
    OnlyNumber,
    TenderComponent,
    SimpleRoutesComponent,
    SimpleRoutesRowComponent,
    PriceSenderComponent,
    CircleRoutesComponent,
    CircleRoutesRowComponent,
    GroupRotuesComponent,
    GroupRoutesRowComponent,
    ContractRoutesComponent,
    ContractRoutesRowComponent,
    CalendarFooterComponent,
    ErrorSnackbarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    MatGridListModule,
    NgxPaginationModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SatDatepickerModule,
    SatNativeDateModule,
    MatSnackBarModule,
    RouterModule.forRoot([
      { path: '', component: TenderComponent, pathMatch: 'full' },
      { path: 'bids', component: TenderBidsComponent, pathMatch: 'full' },
      { path: 'bids/:tab', component: TenderBidsComponent, pathMatch: 'full' }
    ]),
    BrowserAnimationsModule,
    MatIconModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'ua'
    })
  ],
  entryComponents: [CalendarFooterComponent, ErrorSnackbarComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: location.origin }, { provide: 'BASE_URL', useFactory: getBaseUrl },
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    ApiService,
    // SignalrService,
    SignalRService,
    TimerService,
    TranslateService,
    TimeService,
    TranslateStore,
    LocalizationService,
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 5000 } },

  ],

  bootstrap: [AppComponent]
})
export class AppModule { }

export function getBaseUrl() {
  return document.getElementsByTagName('base')[0].href;
}
