﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.Hubs
{
    public interface ITenderTime
    {
        Task GetTenderTime(DateTime dt);  
    }
}
