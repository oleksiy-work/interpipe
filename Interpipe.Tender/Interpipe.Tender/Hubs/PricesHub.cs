﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.Hubs
{
    public class InternalUserType
    {
        public string ConnectionId { get; set; }
    }

    public class PricesHub : Hub<IPricesHub>
    {
        public static ConcurrentDictionary<string, InternalUserType> _users = new ConcurrentDictionary<string, InternalUserType>();

        public override Task OnConnectedAsync()
        {
            _users.TryAdd(Context.ConnectionId, new InternalUserType() { ConnectionId = Context.ConnectionId });
            
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            InternalUserType garbage;

            _users.TryRemove(Context.ConnectionId, out garbage);

            return base.OnDisconnectedAsync(exception);
        }

        public async Task Subscribe(IEnumerable<string> tenders)
        {
            var userName = Context.User.Identity.Name;
            InternalUserType type;
            _users.TryGetValue(userName, out type);
            
            foreach (var tender in tenders)
            {
                await Groups.AddToGroupAsync(type != null ? type.ConnectionId : Context.ConnectionId, tender);
            }
        }

        public void SubscribePriceBid(IEnumerable<string> tenders)
        {
            var userName = Context.User.Identity.Name;
            InternalUserType type;
            _users.TryGetValue(userName, out type);

            foreach (var tender in tenders)
            {
                Groups.AddToGroupAsync(type != null ? type.ConnectionId : Context.ConnectionId, tender);
            }
        }
    }
}
