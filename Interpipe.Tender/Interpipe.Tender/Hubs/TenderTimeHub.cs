﻿using Interpipe.Tender.DbContext;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Interpipe.Tender.Hubs
{
    public class TenderTimeHub : Hub<ITenderTime>
    {
        private readonly InterpipeDbContext _context;
        private readonly string _connectionString;

        public TenderTimeHub(InterpipeDbContext context)
        {
            this._context = context;
            this._connectionString = _context.Database.GetDbConnection().ConnectionString;
        }

        public void GetUtcDate()
        {
            Clients.All.GetTenderTime(GetUtcDateTimeFromSql()); // DateTime.UtcNow);
            while (true)
            {
                System.Threading.Thread.Sleep(1000 * 30);
                Clients.All.GetTenderTime(GetUtcDateTimeFromSql());
            }
        }

        private DateTime GetUtcDateTimeFromSql()
        {
            using var conn = new SqlConnection(this._connectionString);
            var cmd = new SqlCommand("SELECT GETDATE()", conn);
            conn.Open();

            var dt = (DateTime)cmd.ExecuteScalar();

            return dt.ToUniversalTime();
        }
    }
}
