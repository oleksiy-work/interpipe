﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Interpipe.Tender.Models;

namespace Interpipe.Tender.Services
{
    public interface ITenderService
    {
        Task<IEnumerable<Models.Tender>> GetTendersAsync(string userName);

        Task<BidHistory> GetBidHistory(string userName, int uniqueNumber, int lotNumber);

        Task<bool> MakeBid(string userName, int uniqueNumber, int lotNumber,
            decimal price, string currency, string comment, string ip,
            string userType, string browserInfo, int vehiclesAmount,
            DateTime dtSave);

        Task<decimal?> GetBestPrice(int uniqueNumber, int lotNumber);
        Task<bool> MakeBidsAsync(IEnumerable<Bid> bids, List<MakeBidError> errors);
        Task<int> NewTendersAmount(string userName);

        Task<decimal?> GetMinBidPriceConAsync(int uniqueNumber);
    }
}