﻿using System.Threading.Tasks;

namespace Interpipe.Tender.Services.Localization
{
    public interface ILocalizationService
    {
        public Task SetLocalizationAsync(string localization);
        public string GetLocalization();
    }
}
