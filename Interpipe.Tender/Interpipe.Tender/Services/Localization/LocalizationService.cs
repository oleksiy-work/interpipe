﻿using System.Threading.Tasks;
using Interpipe.Tender.Constants;
using Microsoft.Extensions.Logging;

namespace Interpipe.Tender.Services.Localization
{
    public class LocalizationService : ILocalizationService
    {
        private string _localization = Constant.LocalizationUk;

        private readonly ILogger<LocalizationService> _logger;

        public LocalizationService(ILogger<LocalizationService> logger)
        {
            this._logger = logger;
        }
        public string GetLocalization()
        {
            return this._localization;
        }

        public async Task SetLocalizationAsync(string localization)
        {
            this._logger?.LogWarning($"Incoming SetLocalizationAsync request: {localization}");
            var loc = Constant.LocalizationUk;
            switch (localization)
            {
                case Constant.LocalizationEn:
                case Constant.LocalizationRu:
                case Constant.LocalizationUk:
                    loc = localization;
                    break;
            }

            this._logger?.LogWarning($"Apply localization: {loc}");
            var w = Task.Run(() => this._localization = loc);

            await w;
        }
    }
}
