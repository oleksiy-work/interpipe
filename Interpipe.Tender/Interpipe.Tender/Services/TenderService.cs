﻿using Interpipe.Tender.Bids.DbContext;
using Interpipe.Tender.Constants;
using Interpipe.Tender.DbContext;
using Interpipe.Tender.Helpers;
using Interpipe.Tender.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using Interpipe.Tender.Services.Localization;

namespace Interpipe.Tender.Services
{
    public class TenderService : ITenderService
    {
        // private readonly OperationLogger _ol;
        private readonly InterpipeDbContext _context;
        private readonly ILogger<TenderService> _logger;
        private readonly IMemoryCache _cache;
        private readonly BidsDbContext _bidsDbContext;
        private readonly IHubContext<PricesHub> _pricesHubContext;
        private readonly ILocalizationService _localizationService;

        private readonly TenderTypeResolver _tenderTypeResolver =  new TenderTypeResolver();

        private readonly string _connectionString;

        public TenderService(InterpipeDbContext context,
            BidsDbContext bidsContext,
            ILogger<TenderService> logger,
            IMemoryCache cache,
            IHubContext<PricesHub> pricesHubContext,
            ILocalizationService localizationService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _cache = cache ?? throw new ArgumentNullException(nameof(cache));
            _bidsDbContext = bidsContext ?? throw new ArgumentNullException(nameof(bidsContext));
            _pricesHubContext = pricesHubContext ?? throw new ArgumentNullException(nameof(pricesHubContext));
            // _ol = new OperationLogger(_logger);

            _connectionString = _context.Database.GetDbConnection().ConnectionString;
            _localizationService = localizationService;
        }

        public async Task<IEnumerable<Models.Tender>> GetTendersAsync(string userName)
        {
            var localization = this._localizationService.GetLocalization();
            _logger.LogWarning($"Localization: {localization}");
            try
            {
                //_ol.StartMeasureExecutionTime();
                //_ol.Log("GetTendersAsync");
                 var tendersList = await _context.Tenders.FromSqlRaw(
                    "exec [dbo].[GETWEBTENDLGT] @login = {0}, @lang = {1}", userName, String.IsNullOrEmpty(localization) ? Constant.LocalizationUk : localization)
                        .ToListAsync();

                string key = $"{Constant.TenderListForUser}{userName}";


                _cache.Set(key, tendersList, TimeSpan.FromMinutes(2));

                // _ol.LogExecutionTime();

                return tendersList;

            }
            catch (Exception ex)
            {
                _logger.LogError("An error has occurred when trying to get tenders list. {0}", ex);
                Debug.WriteLine(ex);
            }

            return new List<Models.Tender>();
        }

        public async Task<int> NewTendersAmount(string userName)
        {
            //_ol.StartMeasureExecutionTime();
            //_ol.Log("NewTendersAmount");

            string key = $"{Constant.TenderListForUser}{userName}";

            var list = _cache.Get<List<Models.Tender>>(key);

            if (list == null)
                return 0;

            var tendersList = await _context.Tenders.FromSqlRaw(
                    "exec [dbo].[GETWEBTENDLGT] @login = {0}", userName)
                        .ToListAsync();



            int newTendersAmount = 0;

            if (tendersList.Count > list.Count)
            {
                newTendersAmount = tendersList.Count - list.Count;
            }

            // _ol.LogExecutionTime();

            return newTendersAmount;
        }

        public async Task<Models.BidHistory> GetBidHistory(string userName,
            int uniqueNumber, int lotNumber)
        {
            try
            {
                //_ol.StartMeasureExecutionTime();
                //_ol.Log("GetBidHistory");
                string key = string.Format(Constant.TenderLastBidTemplate, userName, uniqueNumber, lotNumber);

                var bid = _cache.Get<Models.BidHistory>(key);

                if (bid == null)
                {
                    var lastBid = await _bidsDbContext.Bids.Where(x => x.UnicalNumber == uniqueNumber && x.LotNumber == lotNumber && x.Login == userName && x.OldSign != "+")
                        .OrderByDescending(x => x.SaveDate)
                        .FirstOrDefaultAsync();

                    if (lastBid != null)
                    {
                        bid = new Models.BidHistory
                        {
                            SaveDate = lastBid.SaveDate,
                            BrowserInfo = lastBid.BrowserInfo,
                            Comment = lastBid.Comment,
                            IpAddress = lastBid.IpAddress,
                            Price = lastBid.Price,
                            VehiclesAmount = lastBid.VehiclesAmount
                        };

                        _cache.Set(key, bid, TimeSpan.FromHours(1));
                    }
                }

                // _ol.LogExecutionTime();

                return bid;
            }
            catch (Exception ex)
            {
                _logger.LogError("An error has occurred when trying to get bids history. {0}", ex);
                Debug.WriteLine(ex);
            }

            return null;
        }

        public async Task<bool> MakeBid(string userName, int uniqueNumber, int lotNumber,
            decimal price, string currency, string comment,
            string ip, string userType, string browserInfo, int vehiclesAmount,
            DateTime dtSave)
        {
            //_ol.StartMeasureExecutionTime();
            //_ol.Log("MakeBid");
            var q = @"";
            try
            {

                q = string.Format(@$"exec [dbo].[ADDBIDTENDLGT] @login=N'{userName}',@undoc={uniqueNumber},@npp={lotNumber},@price={price.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture)},@kval=N'{currency}',@comm=N'{comment}',@scm=N'{userType}',@ipstamp=N'{ip}',@browinfo=N'{browserInfo}',@kol={vehiclesAmount},@dtsave=N'{dtSave.ToString("yyyy-dd-MM HH:mm:ss.fff")}'");


                //var q = new QueryBuilderHelper("ADDBIDTENDLGT")
                // .AppendLogin(userName)
                // .AppendUniqueNumber(uniqueNumber)
                // .AppendLotNumber(lotNumber)
                // .AppendPrice(price)
                // .AppendCurrency(currency)
                // .AppendComment(comment)
                // .AppendUserType(userType)
                // .AppendIp(ip)
                // .AppendBrowserInfo(browserInfo)
                // .AppendVehiclesAmount(vehiclesAmount)
                // .Append("dtsave", dtSave.ToString("yyyy-dd-MM HH:mm:ss.fff"))
                //.Build();

                await using (SqlConnection connection = new SqlConnection(_connectionString))
                {
                    var cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = q;
                    
                    await connection.OpenAsync();

                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(dt);
                    connection.Close();
                    da.Dispose();

                    var row = dt.Rows[0];

                    if(row["result"].ToString() != "SUCCESS")
                    {
                      _logger.LogError($"An error has occurred when trying to make a bid. Query = {q}. Message = {row[2]}.");
                    }
                }
                // _ol.LogExecutionTime();
                var res = await _context.Database.ExecuteSqlRawAsync(q) > 0;
                return res;
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error has occurred when trying make a bid. Query = {q}. Exception = {ex}" );
                Debug.WriteLine(ex);
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
                GC.Collect();
            }

            return false;
        }

        public async Task<bool> MakeBidsAsync(IEnumerable<Models.Bid> bids, List<MakeBidError> errors)
        {
            try
            {
                //_ol.StartMeasureExecutionTime();
                //_ol.Log("MakeBidsAsync");

                if (errors == null)
                    errors = new List<MakeBidError>();

                var enumerable = bids.ToList();
                var tendersAsync = await GetTendersAsync(enumerable.FirstOrDefault()?.Login);
                var tenders = tendersAsync.ToList();
                foreach (var bid in enumerable)
                {
                    if (bid.Price <= 0)
                    {
                        AddBidError(errors, "Цена не может быть меньше или равной 0.", bid.UnicalNumber.ToString());
                        continue;
                    }

                    var enumerableTenders = tenders.ToList();
                    var currentTender = enumerableTenders.FirstOrDefault(x => x.UnicalNumber == bid.UnicalNumber);
                    var tenderTypeId = _tenderTypeResolver.GetTenderTypeId(currentTender?.TenderType);

                    if (tenderTypeId != TenderTypeId.Contract)
                    {
                        if (await CheckIfExistBitWithSamePriceAsync(bid))
                        {
                            AddBidError(errors, "Ставка не может быть одинаковой или больше минимальной.",
                                bid.UnicalNumber.ToString());
                            continue;
                        }
                    }

                    if (currentTender?.EndTime < DateTime.Now)
                    {
                        AddBidError(errors, "Ошибка при подаче тендера, время ставок истекло.", currentTender.Number);
                        continue;
                    }

                    var priceStep = currentTender?.PriceStep ?? 0;
                    if (currentTender != null)
                    {

                        if (currentTender.IsReduction > 0)
                        {
                            if (tenderTypeId ==
                                TenderTypeId
                                    .Round) //currentTender.TenderType.Trim().Equals("RNG", StringComparison.InvariantCultureIgnoreCase))
                            {
                                if (currentTender.BackShippingTender.HasValue)
                                {
                                    var tenderBack = enumerableTenders.FirstOrDefault(x =>
                                        x.UnicalNumber == currentTender.BackShippingTender);

                                    if (tenderBack != null)
                                    {
                                        var backBid = enumerable.FirstOrDefault(x =>
                                            x.UnicalNumber == tenderBack.UnicalNumber);

                                        if (backBid != null)
                                        {
                                            if (backBid.Price <= 0)
                                            {
                                                AddBidError(errors,
                                                    "Ошибка при подаче кольцевого тендера, ставка на обратный маршрут должна быть выше 0.",
                                                    currentTender.Number);
                                                continue;
                                            }

                                            if (bid.Price > currentTender.MinPrice)
                                            {
                                                AddBidError(errors,
                                                    "Ошибка при подаче кольцевого тендера, ставка на первую часть маршрута не может привышать стартовую цену.",
                                                    currentTender.Number, bid.Price, currentTender.MinPrice);
                                                continue;
                                            }

                                            var currentBidBestPrice = await GetBestPrice(currentTender.UnicalNumber,
                                                currentTender.LotNumber);
                                            var backBidBestPrice = await GetBestPrice(tenderBack.UnicalNumber,
                                                tenderBack.LotNumber);

                                            var currentBidBestPriceValue = (currentBidBestPrice ?? currentTender.MinPrice);
                                            var backBidBestPriceValue = (backBidBestPrice ?? tenderBack.MinPrice);

                                            var bidsSum = bid.Price + backBid.Price;

                                            var bidsBestPrice = currentBidBestPriceValue + backBidBestPriceValue -
                                                currentTender.PriceStep ?? 0;

                                            if (bidsSum > bidsBestPrice)
                                            {
                                                AddBidError(errors,
                                                    "Ошибка при подаче кольцевого тендера, сумарная ставка маршрутов выше минимальной ставки.",
                                                    currentTender.Number, bidsSum, bidsBestPrice);
                                                continue;
                                            }

                                            //_bidsDbContext.Add(bid);
                                            //_bidsDbContext.Add(backBid);
                                            await AddBid(bid);
                                            await AddBid(backBid);

                                            continue;
                                        }
                                        else
                                        {
                                            AddBidError(errors,
                                                "Ошибка при подаче кольцевого тендера, ставка на обратный тендер не найдена.",
                                                currentTender.Number);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        AddBidError(errors,
                                            "Ошибка при подаче кольцевого тендера, обратный тендер не найден.",
                                            currentTender.Number);
                                        continue;
                                    }
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else if (
                                tenderTypeId ==
                                TenderTypeId
                                    .Contract) // currentTender.TenderType.Trim().Equals("CON", StringComparison.InvariantCultureIgnoreCase))
                            {
                                await AddBid(bid);
                                continue;
                                //var contractTenderBestBid = await _bidsDbContext.Bids.FirstOrDefaultAsync(x => x.UnicalNumber == bid.UnicalNumber && x.Login == bid.Login);
                                //if (contractTenderBestBid != null)
                                //{
                                //    if (contractTenderBestBid.Price < bid.Price)
                                //    {

                                //    }
                                //    await AddBid(bid);
                                //    continue;
                                //    //if (contractTenderBestBid.Price < bid.Price)
                                //    //{
                                //    //    AddBidError(errors, "Ошибка при подаче контрактного тендера, поданая ставка не должна быть выше предыдущей своей ставки.", currentTender.Number, bid.Price, contractTenderBestBid.Price);
                                //    //    continue;
                                //    //}
                                //    //else
                                //    //{
                                //    //    //_bidsDbContext.Bids.Add(bid);
                                //    //    await AddBid(bid);
                                //    //    continue;
                                //    //}
                                //}
                                //else if (currentTender.MinPrice >= bid.Price)
                                //{
                                //    //_bidsDbContext.Bids.Add(bid);
                                //    await AddBid(bid);
                                //    continue;
                                //}

                            }
                            else if (
                                tenderTypeId ==
                                TenderTypeId
                                    .Group) //currentTender.TenderType.Trim().Equals("GRP", StringComparison.InvariantCultureIgnoreCase))
                            {
                                // var t = await _bidsDbContext.Bids.Where(x => x.UnicalNumber == bid.UnicalNumber && x.Login == bid.Login).ToListAsync();
                                var myLastBid = await _bidsDbContext.Bids
                                    .Where(x => x.UnicalNumber == bid.UnicalNumber && x.Login == bid.Login)
                                    .OrderBy(x => x.Price).FirstOrDefaultAsync();

                                if (myLastBid != null && bid.Price > myLastBid.Price)
                                {
                                    AddBidError(errors,
                                        $"Цена не может превышать уже сделаную ставку с учетом шага ценыю.",
                                        currentTender.Number, bid.Price, currentTender.MinPrice - priceStep);
                                    continue;
                                }
                                else
                                {
                                    if (bid.Price <= currentTender.MinPrice - priceStep ||
                                        bid.Price == currentTender.MinPrice)
                                    {
                                        await AddBid(bid);
                                        continue;
                                    }
                                }
                            }

                            var lastBid = await _bidsDbContext.Bids.Where(x =>
                                    x.UnicalNumber == bid.UnicalNumber && x.LotNumber == bid.LotNumber)
                                .OrderByDescending(x => x.Price).FirstOrDefaultAsync();

                            if (currentTender.MinPrice > 0 &&
                                currentTender.MinPrice - currentTender.PriceStep < bid.Price)
                            {
                                AddBidError(errors, $"Цена должна быть ниже чем стартовая с учетом шана цены.",
                                    currentTender.Number, bid.Price, currentTender.MinPrice - priceStep);
                                continue;
                            }

                            if (lastBid != null)
                            {
                                if (bid.Price > lastBid.Price - currentTender.PriceStep)
                                {
                                    AddBidError(errors, "Цена должна быть ниже чем текущая с учетом шага цены.",
                                        currentTender.Number, bid.Price, lastBid.Price - priceStep);
                                    continue;
                                }
                            }
                        }
                        else
                        {

                            var lastBid = await _bidsDbContext.Bids.Where(x =>
                                    x.UnicalNumber == bid.UnicalNumber && x.LotNumber == bid.LotNumber &&
                                    bid.Login == x.Login)
                                .OrderByDescending(x => x.Price).FirstOrDefaultAsync();

                            if (lastBid != null)
                            {
                                if (bid.Price > lastBid.Price - priceStep)
                                {
                                    AddBidError(errors,
                                        "Ошибка подачи конкурсной заявки. Цена не может быть выше, чем ваша предыдущая цена.",
                                        currentTender.Number, bid.Price, lastBid.Price - priceStep);
                                    continue;
                                }
                            }
                        }

                    }

                    await AddBid(bid);
                }

                var result = await _bidsDbContext.SaveChangesAsync() > 0;

                if (result)
                {

                    foreach (var bid in enumerable)
                    {
                        var ownBidKey = string.Format(Constant.TenderLastBidTemplate, bid.Login, bid.UnicalNumber,
                            bid.LotNumber);
                        var bestBidKey =
                            string.Format(Constant.BidBestPriceTemplate, bid.UnicalNumber, bid.LotNumber);

                        _cache.Remove(ownBidKey);
                        _cache.Remove(bestBidKey);
                    }

                    await _pricesHubContext.Clients.Groups(enumerable.Select(x => $"{x.UnicalNumber}").ToList())
                        .SendAsync("PriceUpdated");
                }

                // _ol.LogExecutionTime();

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                _logger.LogError("An error has occurred when trying make a bids. {0}", ex);
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

                GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;
                GC.Collect();
            }

            return false;
        }

        private async Task AddBid(Models.Bid bid)
        {
            var oldBids = await _bidsDbContext.Bids.Where(x =>
                x.OldSign != "+" && x.UnicalNumber == bid.UnicalNumber
                && x.LotNumber == bid.LotNumber
                && x.Login == bid.Login).ToListAsync();

            oldBids.ForEach(x => x.OldSign = "+");

            _bidsDbContext.Bids.Add(bid);
            _bidsDbContext.Bids.UpdateRange(oldBids);
        }

        private void AddBidError(List<MakeBidError> source, string error, string unicalNumber, decimal enteredPrice = 0,
            decimal price = 0)
        {
            if (source == null)
                source = new List<MakeBidError>();

            source.Add(new MakeBidError
            {
                ErrorMessage = error,
                UnicalNumber = unicalNumber
            });

            StringBuilder errorMessageStringBuilder = new StringBuilder();
            errorMessageStringBuilder.Append($"An error has occurred when trying to make bids. Error message = {error}. Tender number = {unicalNumber}");

            if (enteredPrice > 0)
            {
                errorMessageStringBuilder.AppendLine($"Entered price = {enteredPrice}");
            }
            if (price > 0)
            {
                errorMessageStringBuilder.AppendLine($"Minimal price = {price}");
            }

            _logger.LogError(errorMessageStringBuilder.ToString());
        }

        public async Task<decimal?> GetBestPrice(int uniqueNumber, int lotNumber)
        {
            
            try
            {
                //_ol.StartMeasureExecutionTime();
                //_ol.Log("GetBestPrice");

                string bestBidKey = string.Format(Constant.BidBestPriceTemplate, uniqueNumber, lotNumber);

                var bestBid = _cache.Get<decimal?>(bestBidKey);

                if (bestBid == null)
                {
                    bestBid = (await _bidsDbContext.Bids.Where(x => x.UnicalNumber == uniqueNumber && x.LotNumber == lotNumber && x.OldSign == null)
                        .OrderBy(x => x.Price).FirstOrDefaultAsync())?.Price;
                    if (bestBid != null)
                    {
                        _cache.Set(bestBidKey, bestBid, TimeSpan.FromHours(1));
                    }
                }
               
                // _ol.LogExecutionTime();

                return bestBid;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                _logger.LogError($"An error has occurred when trying to get best price for tender undoc = {uniqueNumber} npp = {lotNumber}. {ex}",
                   ex);
            }

            return null;

        }

        private async Task<bool> CheckIfExistBitWithSamePriceAsync(Models.Bid bid)
        {
            var cnt = await _bidsDbContext.Bids.Where(x =>
               x.UnicalNumber == bid.UnicalNumber
               && x.LotNumber == bid.LotNumber
               && (x.Price == bid.Price || bid.Price > x.Price)).CountAsync();

            return cnt > 0;
        }

        public async Task<decimal?> GetMinBidPriceConAsync(int uniqueNumber)
        {
            try
            {
                var price = await _bidsDbContext.Bids.Where(x => x.UnicalNumber == uniqueNumber).Select(x => x.Price).MinAsync();

                return price;
            }
            catch
            {
                return 0;
            }
        }
    }

    public class MakeBidError
    {
        public string UnicalNumber { get; set; }

        public string ErrorMessage { get; set; }
    }
}
