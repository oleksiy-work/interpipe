﻿using System;
using System.Threading.Tasks;

namespace Interpipe.Tender.Services.Auth
{
    public interface IAuthService
    {
        Task<bool> SignInAsync(Guid ticket);
        Task SignOutAsync();
    }
}