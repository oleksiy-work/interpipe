﻿namespace Interpipe.Tender.Services.Auth
{
    public class AuthServiceResponse
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public AuthServiceResponseData Data { get; set; }
    }

    public class AuthServiceResponseData
    {
        public string Login { get; set; }
    }
}
