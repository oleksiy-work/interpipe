﻿using Interpipe.Tender.Options;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace Interpipe.Tender.Services.Auth
{
    public class AuthService : IAuthService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly ILogger<AuthService> _logger;
        private readonly InterpipeOptions _options;
        
        public AuthService(IHttpContextAccessor httpContext,
            IOptions<InterpipeOptions> options,
            ILogger<AuthService> logger
            )

        {
            _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));

            if (options is null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            _options = options.Value;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

        }
        
        public async Task<bool> SignInAsync(Guid ticket)
        {
            _logger.LogWarning($"start SignInAsync with guid {ticket}");
            try
            {
#if !DEBUG
                AuthServiceResponse responseData;

                ServiceReference1.WebServiceSoapClient client = new ServiceReference1.WebServiceSoapClient(
                    ServiceReference1.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

                var result = await client.ExecuteAsync("_TENDER.GETOUTGOINGDATA", $"{{\"guid\":\"{ticket}\"}}");

                _logger.LogWarning($"Execution _TENDER.GETOUTGOINGDATA result: {result}");

                var normalizedResult = result
                    .Replace("\\", string.Empty)
                    .Replace("\"{", "{")
                    .Replace("\"}\"}", "\"}}");                

                _logger.LogWarning("Authentication result : ");
                _logger.LogWarning(normalizedResult);

                responseData = JsonSerializer.Deserialize<AuthServiceResponse>(normalizedResult);

                _logger.LogWarning($"responseData result: {responseData.Success}");
                _logger.LogWarning($"responseData Data.Login: {responseData.Data?.Login}");

                if (!responseData.Success)
                {
                    await SignOutAsync();
                    return false;
                }
#endif
                var claims = new List<Claim>
                {
#if !DEBUG
                    new Claim(ClaimsIdentity.DefaultNameClaimType, responseData.Data.Login),
#endif
#if DEBUG
                    new Claim(ClaimsIdentity.DefaultNameClaimType, ticket == Guid.Parse("6d1bc3c0-228f-46cc-a4c8-fe36f6795801") ? "xss@it-dnepr.it.ua" : "xss@it-dnepr.it.ua"),
#endif
                    new Claim("Date",DateTime.Now.ToString())
                };

                var claimsIdentity = new ClaimsIdentity(
                   claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    AllowRefresh = true,
                    // Refreshing the authentication session should be allowed.

                    ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(_options?.AuthCookieExpiresInMinutes ?? 60),
                    // The time at which the authentication ticket expires. A 
                    // value set here overrides the ExpireTimeSpan option of 
                    // CookieAuthenticationOptions set with AddCookie.

                    //IsPersistent = true,
                    // Whether the authentication session is persisted across 
                    // multiple requests. When used with cookies, controls
                    // whether the cookie's lifetime is absolute (matching the
                    // lifetime of the authentication ticket) or session-based.

                    //IssuedUtc = <DateTimeOffset>,
                    // The time at which the authentication ticket was issued.

                    //RedirectUri = <string>
                    // The full path or absolute URI to be used as an http 
                    // redirect response value.
                };

                await _httpContext.HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);

                return true;
            }
            catch (Exception ex)
            {
                // _logger.LogError($"An error has occurred when trying to SingIn via web service.\r\n");
                _logger.LogError($"An error has occurred when trying to SingIn via web service.\r\n {ex}");
                Debug.WriteLine(ex);
                await SignOutAsync();
            }

            return false;
        }

        public async Task SignOutAsync()
        {
            try
            {
                await _httpContext.HttpContext.SignOutAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
                _logger.LogError("An error has occurred when trying to sing out", ex);
            }
        }
    }
}
