﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Interpipe.Tender.Models
{
    public class BidHistory
    {
        [Key]
        [Column("dtsave")]
        public DateTime SaveDate { get; set; }

        [Column("pr_old")]
        public string OldSign { get; set; }

        [Column("price",TypeName = "numeric(15,5)")]
        public decimal Price { get; set; }

        [Column("comm")]
        public string Comment { get; set; }

        [Column("ipstamp")]
        public string IpAddress { get; set; }

        [Column("browinfo")]
        public string BrowserInfo { get; set; }

        [Column("scm")]
        public string ProviderType { get; set; }

        [Column("KOL")]
        public int VehiclesAmount { get; set; }
    }
}
