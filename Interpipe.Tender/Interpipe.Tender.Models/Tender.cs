﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interpipe.Tender.Models
{
    public class Tender
    {
        [Key]
        [Column("UNDOC")]
        public int UnicalNumber { get; set; }

        [Column("NPP")]
        public int LotNumber { get; set; }

        [Column("NDM")]
        public string  Number { get; set; }

        [Column("SROK")]
        public DateTime EndTime { get; set; }

        [Column("FORM_NAIM")]
        public string TypeName { get; set; }

        [Column("PR_REDUCTION")]
        public int IsReduction { get; set; }

        [Column("KVAL")]
        public string Currency { get; set; }

        [Column("PRICE_LIMIT",TypeName = "numeric(15,5)")]
        public decimal MinPrice { get; set; }

        [Column("PRICE_STEP", TypeName= "numeric(15,5)")]
        public decimal? PriceStep { get; set; }

        [Column("NORG")]
        public string OwnerName { get; set; }
        
        [Column("CITY_FROM")]
        public string CityFrom { get; set; }

        [Column("COUNTRY_FROM")]
        public string CountryFrom { get; set; }

        [Column("CITY_TO")]
        public string CityTo { get; set; }

        [Column("COUNTRY_TO")]
        public string CountryTo { get; set; }

        [Column("CARGO")]
        public string CargoType { get; set; }

        [Column("TRANSPORT")]
        public string TransportType { get; set; }

        [Column("DTLOAD_FROM")]
        public DateTime LoadingFrom { get; set; }

        [Column("DTLOAD_TO")]
        public DateTime LoadingTo { get; set; }

        [Column("DTUNLOAD")]
        public DateTime UnloadingDate { get; set; }

        [Column("PR_BOOKING")]
        public int IsBooking { get; set; }

        [Column("CUSTOMS")]
        public string Customs { get; set; }

        [Column("COMM")]
        public string CustomerComment { get; set; }

        [Column("WRH_NAME")]
        public string WarehouseName { get; set; }

        [Column("WRH_ADR")]
        public string WarehouseAddress { get; set; }

        [Column("MANAGER_FIO")]
        public string ManagerName { get; set; }

        [Column("MANAGER_TEL")]
        public string ManagerPhone { get; set; }

        [Column("MANAGER_EMAIL")]
        public string ManagerEmail { get; set; }

        [Column("PR_CANBID")]
        public int CanBid { get; set; }

        [Column("ID_COUNTRY_FROM")]
        public string CountryFromISO { get; set; }

        [Column("ID_COUNTRY_TO")]
        public string CountryToISO { get; set; }

        [Column("PAYMENT_CONDS")]
        public string PaymentConditions { get; set; }

        [Column("kol")]
        public short VehiclesAmount { get; set; }

        [Column("tten")]
        public string TenderType { get; set; }

        [Column("status")]
        public string Status { get; set; }

        [Column("undoc_back")]
        public int? BackShippingTender { get; set; }

    }
}
