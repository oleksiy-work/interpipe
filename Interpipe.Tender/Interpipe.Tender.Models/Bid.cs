﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Interpipe.Tender.Models
{
    public class Bid
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
       
        [Column("UNDOC")]
        public int UnicalNumber { get; set; }

        [Column("NPP")]
        public int LotNumber { get; set; }

        [Column("LOGIN")]
        public string Login { get; set; }

        [Column("DTSAVE")]
        public DateTime SaveDate { get; set; }

        [Column("PR_OLD")]
        public string OldSign { get; set; }

        public bool IsOld
        {
            get
            {
                return OldSign != "+";
            }
        }

        [Column("KVAL")]
        public string Currenty { get; set; }

        [Column("COMM")]
        public string Comment { get; set; }

        [Column("DTLOAD")]
        public DateTime? LoadDate { get; set; }

        [Column("SCM")]
        public string ProviderStatus { get; set; }

        [Column("IPSTAMP")]
        public string IpAddress { get; set; }

        [Column("BROWINFO")]
        public string BrowserInfo { get; set; }

        [Column("Price", TypeName = "decimal(18,4)")]
        public decimal Price { get; set; }

        [Column("KOL")]
        public int VehiclesAmount { get; set; }

        public bool IsSynced { get; set; }
    }
}
