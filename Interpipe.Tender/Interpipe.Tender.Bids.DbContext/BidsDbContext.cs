﻿using Interpipe.Tender.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Interpipe.Tender.Bids.DbContext
{
    public class BidsDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public BidsDbContext(DbContextOptions<BidsDbContext> options)
         : base(options)
        {

        }

        public DbSet<Bid> Bids { get; set; }

        public virtual DbSet<Models.Tender> Tenders { get; set; }
    }
}
