﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Interpipe.Tender.Bids.DbContext.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bids",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UNDOC = table.Column<int>(nullable: false),
                    NPP = table.Column<int>(nullable: false),
                    LOGIN = table.Column<string>(nullable: true),
                    DTSAVE = table.Column<DateTime>(nullable: false),
                    PR_OLD = table.Column<string>(nullable: true),
                    KVAL = table.Column<string>(nullable: true),
                    COMM = table.Column<string>(nullable: true),
                    DTLOAD = table.Column<DateTime>(nullable: true),
                    SCM = table.Column<string>(nullable: true),
                    IPSTAMP = table.Column<string>(nullable: true),
                    BROWINFO = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    KOL = table.Column<int>(nullable: false),
                    IsSynced = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bids", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bids");
        }
    }
}
