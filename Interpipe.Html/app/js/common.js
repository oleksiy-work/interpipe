jQuery(function($){
   
 // Mask for phone
 if ($('#phone').length > 0) {
    $("#phone").mask("+38(999) 999-9999");
} 
if ($('#data').length > 0) {
    $("#data").mask("99.99.9999", {placeholder: "дд.мм.гггг" });
} 
// Mask the end

// Validation email checking
$("#validateEmailField").keyup(function(){
    var email = $("#validateEmailField").val();
  
    if(email != 0)
    {
    if(isValidEmailAddress(email))
    {
        $("#validEmail").html('Email введен верно!');
        $("#validEmail").css({color: "green"});
    } else {
        $("#validEmail").html('Email введен не верно!');
        $("#validEmail").css({color: "red"});
    }
    } else {
        $("#validEmail").html('Вы ничего не ввели!'); 
        $("#validEmail").css({color: "red"});
    }
  
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
        }
// Validation email checking the end

// Validation sizes checking 
$(".sizes__input").bind("change keyup input click", function() {
    if (this.value.match(/[^0-9]/g)) {
    this.value = this.value.replace(/[^0-9]/g, '');
    }
    })
    

// Validation sizes checking the end
    $('#menu-click').click(function() {
        var windowSize = screen.width;
        if(windowSize < 992) {
            $('#home-header__nav').addClass('home-header__nav-open');
            $('.overlay-hidden').addClass(' overlay');
           
        };
    });
    $('.overlay-hidden').click(function() {
        var windowSize = screen.width;
        if(windowSize < 992) {
            $('#home-header__nav').removeClass('home-header__nav-open');
            $('.overlay-hidden').removeClass(' overlay');
           
        };
    });

    $('.small-slider').slick({
        touchMove: false,
        asNavFor: '.common-slider',
        speed: 420
    });
    $('.slider-bg').slick({
        touchMove: false,
        speed: 400,
        asNavFor: '.common-slider',
        prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="38.691" height="24" viewBox="0 0 38.691 24"><path id="arrow-left" d="M36.709,10.018H5.411l7.468-7.467A1.674,1.674,0,1,0,10.511.183L.183,10.508a1.674,1.674,0,0,0,0,2.368L10.511,23.2a1.674,1.674,0,1,0,2.368-2.368L5.411,13.367h31.3a1.674,1.674,0,1,0,0-3.349Z" transform="translate(0.307 0.308)"/></svg></button>', 
        nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="45.14" height="28" viewBox="0 0 45.14 28"><path id="arrow-right" d="M42.879,11.739H6.364l8.713-8.711A1.954,1.954,0,0,0,12.314.265L.265,12.311a1.952,1.952,0,0,0,0,2.762L12.314,27.12a1.954,1.954,0,0,0,2.763-2.762L6.364,15.646H42.879a1.954,1.954,0,1,0,0-3.907Z" transform="translate(44.833 27.692) rotate(180)"/></svg></button>'
    });
    $('.slider-text').slick({
        touchMove: false,
        asNavFor: '.common-slider',
        dots: true,
        infinite: true,
        speed: 390,
        fade: true,
        cssEase: 'linear'
    });
    $('.slider-maraphone').slick({
        infinite: false,
    speed: 300,
    slidesToShow: 1.2,
    slidesToScroll: 1,
    initialSlide: 0,
    });

    
new WOW().init();

$(window).scroll(function() {
    var top = $(document).scrollTop();
    if (top > 5) {
        $('.header').addClass('header-scroll');
        
    }
    else {
        $('.header').removeClass('header-scroll');
    }
});
             
$('.front').click(function() {
   $(this).parent().parent().addClass('experts-click');
});

$('.experts__open-closeButton').click(function() {
    $('.experts').removeClass('experts-click');
 });

 $(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $(".experts.experts-click"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('.experts').removeClass('experts-click'); // скрываем его
    }
});


    var offset = $('#fixed-main__link').offset();
    offset = offset/2;
  $(window).scroll(function() {
      if ($(window).scrollTop() > offset.top) {
          $('#fixed-main__link').addClass('fixed');
      }
      else {
          $('#fixed-main__link').removeClass('fixed');
      }
  });
  $('.more').click(function() {
      $(this).parent().parent().toggleClass('more-open');
});

$('#check1').click(function() {
    $('.overlay-hidden').toggleClass('overlay');
    // $('.header__nav-main').toggleClass('header__nav-open');
});

$('.experts__item-link').bind("click", function(e) {
    e.preventDefault(); // не перекидывает в начало страницы
 });

 $('#video-close').click(function() {
    $('.profile__video-report').toggle('slow');
});
$('#marafon-close').click(function() {
    $('.profile__maraphone').toggle('slow');
    $('.slider-maraphone').toggle('slow');
});

$('.mobile-footer').click(function() {
    $('.menu-mobile-boottom').toggleClass('menu-mobile-open');
    $(this).toggleClass('mobile-footer-open');
    $('body').toggleClass('body-scroll-none');
});


$('[data-fancybox]').fancybox({
    baseClass: "registr-poppup"
});
$('#sex-select').selectric({
    disableOnMobile: false,
    nativeOnMobile: false,
    responsive: true,
});




//Scroll of the calendar


 if($(window).width() < 768) {
    var maraphone = $('.marafon-calendar__common');
    var maraphoneBlockWidth = maraphone.width();
    var amountOfMaraphoneDays = $('#marafon-calendar').find('.marafon-calendar__common').length;
    var maraphoneBlockMargin = 15;
    var totalMaraphoneWidth = (maraphoneBlockWidth + maraphoneBlockMargin)*amountOfMaraphoneDays;
    $('#marafon-calendar').width(totalMaraphoneWidth);
 }
 else {
    var maraphone = $('.marafon-calendar__common');
    var maraphoneBlockWidth = maraphone.width();
    var amountOfMaraphoneDays = $('#marafon-calendar').find('.marafon-calendar__common').length;
    var maraphoneBlockMargin = 25;
    var totalMaraphoneWidth = (maraphoneBlockWidth + maraphoneBlockMargin)*amountOfMaraphoneDays;
    $('#marafon-calendar').width(totalMaraphoneWidth);
 }

 $('.marafon-calendar__button-next').click(function () {
    var scrollValue = $('.marafon-calendar__wrap').scrollLeft();
    console.log(scrollValue);
   
    $('.marafon-calendar__wrap').animate({scrollLeft: scrollValue + 150},400); 
    var scrollValue = $('.marafon-calendar__wrap').scrollLeft();
    var scrollBoolean = scrollValue > 0;
    if (scrollBoolean) {
        $('.marafon-calendar__button-prev').show('slow');
        console.log(scrollBoolean);
    }
});
$('.marafon-calendar__button-prev').click(function () {
    var scrollValue = $('.marafon-calendar__wrap').scrollLeft();
    console.log(scrollValue);
   
    $('.marafon-calendar__wrap').animate({scrollLeft: scrollValue - 150},400); 
    var scrollValue = $('.marafon-calendar__wrap').scrollLeft();
    var scrollBoolean = scrollValue < 150;
    if (scrollBoolean) {
        $('.marafon-calendar__button-prev').hide('slow');
        console.log(scrollBoolean);
    }
});

//Grafics
Array.prototype.max = function(){
    var max = parseInt(this[this.length-1]), el;
    for(var i=this.length-2; i>=0; i--){
        el = parseInt(this[i]);
        if(el>max){
            max = el;
        }
    }
    return max;
};

Chart.plugins.register({
    beforeDraw: function(chartInstance, easing) {
      var ctx = chartInstance.chart.ctx;
      ctx.fillStyle = '#F5F5F5'; // your color here
  
      var chartArea = chartInstance.chartArea;
      ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
    }
  });
 
  var myChartWeightData =  [65, 65, 64.5, 64.3, 64.1,64.1, 63,63.3,62.7, 62.5, 62.2, 62, 62, 62, 61.7, 61.4, 61.2, 61, 60.7, 60.4];
    minValuemyChartWeight =myChartWeightData.max() - 20;

  var ctx = document.getElementById("myChartWeight").getContext('2d');
    gradient = ctx.createLinearGradient(0, 0, 0, 450);

    gradient.addColorStop(0.1, 'rgba(255, 111,97, 1)');
    gradient.addColorStop(0.45, 'rgba(255, 255, 255, 0.2)');
    gradient.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
  if(ctx){
      Chart.defaults.global.responsive = false;
          var myChartWeight = new Chart(ctx, {
          type: 'line',
          data: {
          labels: [" ", "01", "02", "03", "04", "05", "06", "07","08","09","10","11","12","13","14","15","16","17","18","19", "20"],
              datasets: [{
                  data:  myChartWeightData,
                  fill: true,
                  lineTension: 0,
                  pointBackgroundColor: "#fff",
                  pointBorderColor: 'rgba(255, 111, 97, 1)',
                  pointBorderWidth: 2,
                
                backgroundColor: gradient,
                  borderWidth: 1,
                  }
              ]
          
          },
      
          elements: { 
              line : {
                  tension: 0,
                  borderWidth: 7
              },
              point: {
                  display: true,
                  radius: 3,
                  pointStyle: "circle",
                  backgroundColor: "red",
              }
          },
          options: {
              maintainAspectRatio: false,
              layout: {
                  padding: {
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0
                  }
              },
              legend: {
                  display: false,
                  labels: {
                      fontColor: '#ffffff'
                   }
              },
          
              responsive: true,
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero:true,
                          min: minValuemyChartWeight, 
                        //   max: 125,
                          stepSize: 1,
                          fontColor:'#C0CBCC',
                          fontSize: 10,
                      },
                      gridLines: {
                          color: "#fff",
                          lineWidth:2
                      }
                  }],
                  xAxes: [{
                      ticks: {
                          beginAtZero:true,
                        //   min:0, 
                        //   max: 20,
                          stepSize: 5, 
                          fontColor:'#C0CBCC',
                          fontSize: 10,
                      },
                      gridLines: {
                          color: "#fff",
                          lineWidth:2,
                          drawBorder: true,
                          zeroLineWidth: 1,
                          zeroLineColor: 'red',
                          offsetGridLines: false
                      }
                  }],
              }
          }
          
      
      });
  }
//Weight grafic the end

var myChartWaistData =  [80, 81, 80, 78, 76,74, 73,75,72, 71, 68, 64];
    minValuemyChartWaist =myChartWaistData.max() - 20;

var ctx1 = document.getElementById("myChartWaist").getContext('2d');
gradient = ctx1.createLinearGradient(0, 0, 0, 450);

gradient.addColorStop(0.1, 'rgba(255, 111,97, 1)');
gradient.addColorStop(0.45, 'rgba(255, 255, 255, 0.2)');
gradient.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
if(ctx1){
  Chart.defaults.global.responsive = false;
      var myChartWaist = new Chart(ctx1, {
      type: 'line',
      data: {
      labels: [" ", "01", "02", "03", "04", "05", "06", "07","08","09","10","11","12","13","14","15","16","17","18","19", "20"],
          datasets: [{
              data: myChartWaistData,
              fill: true,
              lineTension: 0,
              pointBackgroundColor: "#fff",
              pointBorderColor: 'rgba(255, 111, 97, 1)',
              pointBorderWidth: 2,
              backgroundColor: gradient,
              borderWidth: 1,
              }
          ]
      
      },
  
      elements: { 
          line : {
              tension: 0,
              borderWidth: 7
          },
          point: {
              display: true,
              radius: 3,
              pointStyle: "circle",
              backgroundColor: "red",
          }
      },
      options: {
        maintainAspectRatio: false,
          layout: {
              padding: {
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0
              }
          },
          legend: {
              display: false,
              labels: {
                  fontColor: '#ffffff'
               }
          },
      
          responsive: true,
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      min: minValuemyChartWaist, 
                    //   max: 125,
                      stepSize: 1,
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2
                  }
              }],
              xAxes: [{
                  ticks: {
                      beginAtZero:true,
                    //   min:0, 
                    //   max: 20,
                      stepSize: 5, 
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2,
                      drawBorder: true,
                      zeroLineWidth: 1,
                      zeroLineColor: 'red',
                      offsetGridLines: false
                  }
              }],
          }
      }
      
  
  });
}

//Waist the end
var myChartChestData =  [60, 65, 60, 63, 58,52, 50,50,49, 48, 46, 50];
    minValuemyChartChest = myChartChestData.max() - 20;

var ctx2 = document.getElementById("myChartChest").getContext('2d');
gradient = ctx2.createLinearGradient(0, 0, 0, 450);

gradient.addColorStop(0.1, 'rgba(255, 111,97, 1)');
gradient.addColorStop(0.45, 'rgba(255, 255, 255, 0.2)');
gradient.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
if(ctx2){
  Chart.defaults.global.responsive = false;
      var myChartChest = new Chart(ctx2, {
      type: 'line',
      data: {
      labels: [" ", "01", "02", "03", "04", "05", "06", "07","08","09","10","11","12","13","14","15","16","17","18","19", "20"],
          datasets: [{
              data: myChartChestData,
              fill: true,
              lineTension: 0,
              pointBackgroundColor: "#fff",
              pointBorderColor: 'rgba(255, 111, 97, 1)',
              pointBorderWidth: 2,
              backgroundColor: gradient,
              borderWidth: 1,
              }
          ]
      
      },
  
      elements: { 
          line : {
              tension: 0,
              borderWidth: 7
          },
          point: {
              display: true,
              radius: 3,
              pointStyle: "circle",
              backgroundColor: "red",
          }
      },
      options: {
            maintainAspectRatio: false,
          layout: {
              padding: {
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0
              }
          },
          legend: {
              display: false,
              labels: {
                  fontColor: '#ffffff'
               }
          },
      
          responsive: true,
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      min: minValuemyChartChest, 
                    //   max: 125,
                      stepSize: 1,
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2
                  }
              }],
              xAxes: [{
                  ticks: {
                      beginAtZero:true,
                    //   min:0, 
                    //   max: 20,
                      stepSize: 5, 
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2,
                      drawBorder: true,
                      zeroLineWidth: 1,
                      zeroLineColor: 'red',
                      offsetGridLines: false
                  }
              }],
          }
      }
      
  
  });
}
//Chest the end


var myChartHipsData =  [70, 65, 60, 63, 58,52, 50,50,55, 53, 52, 50];
    minValuemyChartHips = myChartHipsData.max() - 20;
   

var ctx3 = document.getElementById("myChartHips").getContext('2d');
gradient = ctx3.createLinearGradient(0, 0, 0, 450);

gradient.addColorStop(0.1, 'rgba(255, 111,97, 1)');
gradient.addColorStop(0.45, 'rgba(255, 255, 255, 0.2)');
gradient.addColorStop(1, 'rgba(255, 255, 255, 0.2)');
if(ctx3){
  Chart.defaults.global.responsive = false;
      var myChartHips = new Chart(ctx3, {
      type: 'line',
      data: {
      labels: [" ", "01", "02", "03", "04", "05", "06", "07","08","09","10","11","12","13","14","15","16","17","18","19", "20"],
          datasets: [{
              data: myChartHipsData,
              fill: true,
              lineTension: 0,
              pointBackgroundColor: "#fff",
              pointBorderColor: 'rgba(255, 111, 97, 1)',
              pointBorderWidth: 2,
              backgroundColor: gradient,
              borderWidth: 1,
              }
          ]
      
      },
  
      elements: { 
          line : {
              tension: 0,
              borderWidth: 7
          },
          point: {
              display: true,
              radius: 3,
              pointStyle: "circle",
              backgroundColor: "red",
          }
      },
      options: {
        maintainAspectRatio: false,
          layout: {
              padding: {
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0
              }
          },
          legend: {
              display: false,
              labels: {
                  fontColor: '#ffffff'
               }
          },
      
          responsive: true,
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true,
                         min: minValuemyChartHips, 
                    //   max: 125,
                      stepSize: 1,
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2
                  }
              }],
              xAxes: [{
                  ticks: {
                      beginAtZero:false,
                    //   min:  "", 
                    //   max: 20,
                      stepSize: 1, 
                      fontColor:'#C0CBCC',
                      fontSize: 10,
                  },
                  gridLines: {
                      color: "#fff",
                      lineWidth:2,
                      drawBorder: true,
                      zeroLineWidth: 1,
                      zeroLineColor: 'red',
                      offsetGridLines: false
                  }
              }],
          }
      }
      
  
  });
}
//Hips the end


 
    
});