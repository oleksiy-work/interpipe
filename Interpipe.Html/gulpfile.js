var gulp = require('gulp'),
 sass = require('gulp-sass'),
 concat = require('gulp-concat'),
 uglify = require('gulp-uglifyjs'),
 cssnano = require('gulp-cssnano'),
 rename = require('gulp-rename'),
 del = require('del'),
 imagemin = require('gulp-imagemin'),
 imagequant = require('imagemin-pngquant'),
 cache = require('gulp-cache'),
 autoprefixer = require('gulp-autoprefixer');
 fileinclude = require('gulp-file-include'),

 gulp.task('html', function () {
    return gulp.src('app/html/*.html')
        .pipe(fileinclude())
        .pipe(gulp.dest('app'));
});

gulp.task('sass', function() {
return gulp.src('app/sass/**/*.scss')
.pipe(sass())
.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
.pipe(gulp.dest('app/css'))
});

gulp.task('styles', function() {
    return gulp.src([
        'app/css/libs.css',
        'app/css/main.css'
    ])
    .pipe(cssnano())//Сжимает минимизирует файлы
    .pipe(rename({suffix: '.min'})) // Задаем название нового минимизированного файла
    .pipe(gulp.dest('app/css'))
    });
    

gulp.task('scripts', function() {
    return gulp.src([
        // 'app/libs/jquery/dist/jquery.min.js',
        'app/libs/jquery-3.4.1.js',
        'app/libs/bootstrap/dist/js/bootstrap.min.js',
        'app/libs/slick-carousel/slick/slick.min.js',
        'app/libs/wowjs/wowjs/wow.min.js',
        'app/libs/chart.js/dist/Chart.min.js',
        'app/libs/jquery-circle-progress/dist/circle-progress.js',
        'app/libs/fancybox-master/dist/jquery.fancybox.min.js',
        'app/libs/jquery.scrollbar-gh-pages/jquery.scrollbar-gh-pages/jquery.scrollbar.min.js',
        'app/libs/selectric/src/jquery.selectric.js',
        'app/libs/jquery.maskedinput.js'
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/js'))
    });

gulp.task('clean', function() {
   return del('dist');  
});

gulp.task('clear', function() {
    return cache.clearAll();  
 });

gulp.task('img', function() {
    return gulp.src('app/img/**/*')
    .pipe(cache(imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}], 
        une: [imagequant()]
    })))
    .pipe(gulp.dest('dist/img'))
    });


gulp.task('watch', gulp.series('html','sass', 'styles', 'scripts', function() {
    gulp.watch('app/html/**/*.html', gulp.series('html'));
    gulp.watch('app/sass/**/*.scss', gulp.series('sass'));
    gulp.watch(['app/css/main.css', 'app/css/libs.css'], gulp.series('styles'));
    gulp.watch('app/libs/**/*.js', gulp.series('scripts'));
    })
);

gulp.task('build', gulp.series( 'html', 'img', 'sass','styles', 'scripts', function() {
    var buildCss =  gulp.src([
        'app/css/main.min.css',
        'app/css/libs.min.css'
    ])
    .pipe(gulp.dest('dist/css'));

    var buildFonts = gulp.src([
        'app/fonts/**/*'
    ])
    .pipe(gulp.dest('dist/fonts'));

    var buildJs = gulp.src('app/js/**/*')
    .pipe(gulp.dest('dist/js'));

    var buildHtml = gulp.src('app/*.html')
    .pipe(gulp.dest('dist'));

    return buildCss;
    return buildFonts;
    return buildJs;
    return buildHtml;
}));

